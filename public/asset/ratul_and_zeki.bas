# INCLUDE <attr.bas>

Rem array de las fases
DIM pantallas(21,8,14) As byte => {_
{_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } },_
{_	
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0 },_
	{ 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } },_
{_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1 },_
	{ 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1 },_
	{ 0, 0, 5, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0 },_
	{ 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 5, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1 },_
	{ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0 },_
	{ 1, 0, 0, 0, 0, 0, 1, 0, 0, 8, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0 },_
	{ 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2 },_
	{ 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1 },_
	{ 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 0, 0, 5, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0 },_
	{ 0, 2, 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 9, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0 },_
	{ 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0 },_
	{ 0, 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0 },_
	{ 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 2, 0, 0, 0, 1 },_
	{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9, 0, 0, 0 },_
	{ 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 2, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 },_
	{ 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0 }},_
{_
	{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0 },_
	{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0 },_
	{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0 },_
	{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0 },_
	{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0 },_
	{ 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0 },_
	{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0 },_
	{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0 },_
	{ 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1 },_
	{ 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 },_
	{ 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 2, 0, 1, 2, 0, 1, 2, 0, 1, 0, 1, 0, 0, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0 }},_
{_
	{ 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0 },_
	{ 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0 },_
	{ 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 1, 0, 0, 0 },_
	{ 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },_
	{ 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0 },_
	{ 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 7, 1, 0, 0, 1, 0, 0, 10, 0, 0, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1 },_
	{ 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 1, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },_
	{ 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 10, 0, 0, 1 },_
	{ 1, 0, 0, 0, 3, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 1, 1, 0, 2, 0, 0, 0, 1, 0, 0 },_
	{ 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0 },_
	{ 1, 0, 2, 1, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 0, 0, 0, 0, 0 },_
	{ 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0 }},_
{_
	{ 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 4, 0, 2, 0, 2, 0, 0, 10, 0, 0, 1 },_
	{ 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 7, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 1, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0 },_
	{ 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0 },_
	{ 0, 0, 2, 0, 0, 0, 0, 0, 0, 12, 0, 0, 0, 0, 0 },_
	{ 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 12, 1, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1 },_
	{ 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 1, 0, 0, 0, 1, 1, 0, 9, 0, 1, 1, 1, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 1, 1, 1, 0, 0, 2, 0, 0, 7, 0, 0, 2, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1 },_
	{ 0, 1, 1, 1, 0, 9, 0, 0, 0, 0, 0, 0, 12, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0 },_
	{ 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },_
	{ 2, 0, 0, 5, 0, 0, 2, 0, 1, 0, 0, 0, 0, 1, 1 },_
	{ 0, 1, 0, 0, 2, 1, 0, 0, 2, 0, 1, 1, 0, 2, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_
{_
	{ 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1 },_
	{ 0, 1, 0, 0, 0, 1, 0, 0, 0, 12, 0, 0, 2, 0, 0 },_
	{ 0, 0, 0, 7, 0, 0, 2, 0, 0, 1, 1, 0, 1, 1, 0 },_
	{ 0, 1, 1, 1, 1, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0 },_
	{ 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0 },_
	{ 0, 0, 0, 5, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0 },_
	{ 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }},_	
{_
	{ 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 11, 0, 0, 0 },_
	{ 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0 },_
	{ 0, 1, 0, 0, 0, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0 },_
	{ 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2 },_
	{ 2, 0, 1, 1, 1, 0, 0, 0, 2, 1, 0, 12, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0 },_
	{ 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 1, 0, 0, 0, 0 },_
	{ 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0 },_
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }}_
}

dim bonuslist (21) as integer => {300,350,400,475,475,400,425,450,510,530,535,540,550,575,700,750,650,700,825,800,850,750}

Rem declara los UDG

Dim font (167) As uByte => { _
	222,223,223,  0,251,251,123,  0, _
	 60,102,195,153,153,195,102, 60, _
	128,192,231,255,225,237,237,225, _
	  1,  3,231,255,135,183,183,135, _
	255,255,124, 57, 95,207,  6,  6, _
	255,159, 62,252,250,242, 96,112, _
	255,249,124, 63, 95, 79,  6, 14, _
	255,255, 62,156,250,243, 96, 96, _
	  0,  0,  1,  1,  1,  1,  0,  0, _
	 60,254,182,252,254,240,254,252, _
	 16, 32, 64,192,192,255,127, 63, _
	240,248,124, 62, 62,254,248,224, _
	  3,  7,  6,  3,  7,  0,  7,  3, _
	224,240,216,248,248,248,240,240, _
	 15, 31, 63,115,225,237,237,225, _
	240,248,252,206,135,183,183,135, _
	245,255,253,206,135,183,183,135, _
	243,127, 63, 31, 15,  6, 30, 30, _
	207,254,252,250,246,102, 96, 96, _
	243,127, 63, 95,111,102,  6,  6, _
	207,254,252,248,240, 96,120,120 _
}
Poke uInteger 23675, @font (0)

rem declara variables

dim n, m, murox, muroy, contador1, contador2, fr, fp as Byte
dim jugador1x, jugador1y, jugador2x, jugador2y as Ubyte
dim enemigo1x, enemigo1y, enemigo2x, enemigo2y, direccion1, direccion2 as Ubyte
Dim sentido1, sentido2 as byte
dim lenemigo1x, lenemigo1y, lenemigo2x, lenemigo2y, lsentido1, lsentido2 as Ubyte
dim ldireccion1, ldireccion2 as Byte
dim atrapado1, atrapado2 as Ubyte
dim stage, laststage, life  as byte
dim score,hiscore as Uinteger
dim bonus, i as integer
dim a$,b$,c$,d$,e$,pared$, muro$, trampa$, alea1$,alea2$,lin1$,lin2$ as String

LET a$="\O\P": LET b$="\R\S": LET c$="\O\Q"

let fr=0
let fp=0
let muro$="\A\A"
let pared$="\A"
let trampa$="\B"
let alea1$="\C\D"
let alea2$="\E\F"
let lin1$="\I\J"
let lin2$="\K\L"
let laststage=0
let hiscore=2000

rem  declara funciones y subrutinas
DECLARE SUB HALTLOOP( wait AS UBYTE )
DECLARE SUB pintanivel( nivel as BYTE )

rem pantalla de inicio
inicio:
let life=5
let score=0
let jugador1x=17: let jugador1y=1
let jugador2x=17: let jugador2y=29
let stage=0
border 0
paper 0
ink 7
cls
print at 0,1;ink 1;"\::\''\:. \.:\::\:. \::\::\:: \:: \:: \::"
print at 1,1;ink 1;"\::\..\:' \:: \::  \::  \:: \:: \::"
print at 2,1;ink 1;"\::\'.  \::\::\::  \::  \:: \:: \::"
print at 3,1;ink 1;"\:: \'. \:: \::  \::  \':\::\:' \::\::\::"

print at 5,17; ink 2;"\::\::\:: \::\::\:: \::  \.' \::"
print at 6,17; ink 2;" \ .\'  \::\..  \:: \.'  \::"
print at 7,17; ink 2;"\ .\'   \::\''  \::\::\.   \::"
print at 8,17; ink 2;"\::\::\:: \::\::\:: \:: \ '\.. \::"

print at 4,9; ink 3;"\.."
print at 5,8; ink 3;"\ : \: "
print at 6,9; ink 3;"\::"
print at 7,8; ink 3;"\.' \'. \.'"
print at 8,7; ink 3;"\ :   \::"
print at 9,7; ink 3;" \'.\..\.' \'." 

PRINT AT 11,12; INK 1;a$; AT 12,12; INK 1;b$ ; AT 11,18; INK 2;c$; AT 12,18; INK 2;b$
print at 12,7;ink 7;"Keys"; AT 14,8; INK 7;"Q  "; INK 1;"Up "; INK 2;"   Up"; AT 15,8; INK 7;"A  "; INK 1;"Down "; INK 2;" Down"; AT 16,8; INK 7;"O  "; INK 1;"Right "; INK 2;"Left"; AT 17,8; INK 7;"P  "; INK 1;"Left "; INK 2;" Right"; 
print at 19,0; ink 6;"Push ";inverse 1;"any key";inverse 0;" start in ";ink 5;"Stage 0";
if laststage>0 then
	print at 20,0;ink 6;"Push ";inverse 1;"0";inverse 0;" to start in ";ink 5;"Stage ";laststage;
end if
print at 22,8; ink 7;"\* 2011 Salvacam";
pause 0
if inkey$="0" then
	let stage=laststage
end if
rem sonido de inicio

rem **********************************************************
rem ****** definicion de fases *******************************
rem **********************************************************
rem comprobar bonus
fases:

rem pantalla final
if stage=22 then
	cls
	PRINT "\{p0}\{i0}\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\{p6}\:'\{i3}\ .\..\{i0}\ '\''\''\''\' \{i3}\..\..\{i0}\ '\{p0}\::\::\::\::\::\::\::\::\{p6}\:'\{i5}\..\..\..\..\..\{i0}\ '\{p0}\::\::\::\::\::\::\{i6}\ :\{p2}\{i3}\'.\{i2}\::\{i3}\''\{p6}\:.\{p3}\::\{i6}\''\{i2}\..\{p2}\::\{i3}\ '\{p6}\'.\{i0}\ :\{p0}\::\::\::\::\::\::\{p6}\:'\{i5}\.'\{p1}\{i1}\::\::\::\::\::\{i5}\''\{p0}\{i6}\'.\{i0}\::\::\::\::\{p6}\:'\' \{i3}\.'\{p2}\''\''\{i2}\::\::\{i3}\''\..\. \{i2}\::\{i3}\ :\{p6}\{i0}\ :\{p0}\::\::\::\::\::\{p6}\:'\{i5}\.'\{p1}\{i1}\::\::\::\::\::\::\::\{i5}\ '\{p0}\{i6}\'.\{i0}\::\::\::\{i6}\.:\{p3}\' \{p2}\{i2}\::\::\::\::\{i3}\''\:.\{i2}\::\{p3}\.'\{p2}\::\{i3}\ :\{p6}\{i0}\ :\{p0}\::\::\{i6}\.:\''\''\{p5}\' \{p1}\{i1}\::\::\::\::\::\::\{i5}\''\:.\{i1}\::\{i5}\ :\{p0}\{i6}\''\''\:.\{p3}\' \{p2}\{i2}\::\::\::\::\::\::\{i3}\''\{i2}\::\::\{p6}\{i3}\:'\''\{i0}\ :\{p0}\::\{i6}\ :\' \{i7}\.:\{p7}\::\{p5}\{i1}\ :\{p0}\' \ :\{p1}\::\{i0}\.'\{p7}\''\{p1}\..\{i1}\::\{i5}\''\{i1}\::\{p5}\{i0}\.:\{p7}\' \{i7}\::\{p0}\{i3}\ :\{p2}\: \{i0}\'.\{i2}\::\{p0}\:'\{i7}\..\{i2}\ '\':\{p2}\::\::\::\{i3}\':\{p6}\{i0}\ :\{p0}\::\::\{i6}\ :\{i7}\ :\{p7}\::\::\{p5}\{i0}\ :\{p7}\ .\.:\{p1}\ :\{p7}\ .\..\ '\{p1}\: \{i1}\::\{p0}\''\{p7}\' \{i7}\::\{p0}\:'\{i3}\ '\{i7}\ :\''\{i2}\ :\{i7}\ :\''\':\{i2}\ :\{p2}\::\::\::\{i3}\ '\{p6}\. \{p0}\{i0}\::\::\{i6}\ :\{i7}\ '\{p7}\::\{i0}\ :\{p5}\ :\{p7}\ :\.:\{p1}\ :\{p7}\ :\.:\{i7}\::\{p0}\{i1}\ :\{p1}\::\{p0}\{i5}\'.\{i7}\ :\{p7}\::\{i0}\ :\{i2}\ :\{i0}\: \:.\{p2}\: \{p7}\: \:.\: \{p2}\: \{i2}\::\{p0}\''\ '\{p3}\':\{i6}\ :\{p0}\{i0}\::\::\{i6}\ '\'.\{i7}\ '\''\{i5}\: \{i7}\:.\. \{i1}\: \{i7}\:.\..\{p7}\::\{p0}\{i1}\ :\{p1}\::\{i5}\ :\{p0}\{i7}\ :\{p7}\::\{i0}\ '\{i2}\ '\{i0}\: \''\{p2}\: \{p7}\: \''\' \{p2}\: \.'\{p7}\{i7}\::\::\{p3}\{i0}\'.\{i6}\ :\{p0}\..\. \ .\:'\''\''\{i1}\ '\{i7}\ '\{i1}\ :\:.\{i7}\ '\''\{i1}\..\{p1}\::\::\{i5}\ '\{p7}\{i0}\'.\..\..\{p3}\: \{p2}\ '\{p7}\.'\{p2}\{i2}\::\{i0}\ '\{p7}\..\.'\{p2}\':\{p7}\{i7}\::\{p0}\:'\{p7}\::\::\{p3}\{i0}\: \{i2}\. \{p6}\{i0}\ :\: \{p7}\: \{i7}\::\::\{i0}\ '\{p1}\{i5}\'.\{i1}\::\::\::\::\::\::\::\::\{i5}\ '\''\{p6}\:'\{i6}\::\{p3}\{i2}\ '\''\''\''\':\{p2}\::\{i0}\ '\{p7}\..\.'\' \{i7}\::\{p2}\{i0}\: \{p6}\{i3}\:'\{i0}\ :\: \{p7}\: \{i7}\::\::\::\{i0}\ '\{p1}\{i5}\'.\{i1}\::\::\::\::\::\::\::\::\{i5}\.'\{p0}\{i6}\':\:'\{i7}\..\..\..\..\{i3}\''\{p2}\. \{i2}\::\{p7}\{i0}\'.\..\..\..\{p3}\: \{p6}\ .\{p0}\::\{i6}\ '\:.\{i7}\':\{p7}\::\::\::\{i0}\ :\{p5}\{i1}\ :\{p1}\::\::\::\{p5}\:'\''\''\{i6}\ .\{p0}\' \ :\{i7}\ '\{p7}\::\::\::\::\::\{p3}\{i0}\'.\{i2}\':\{p2}\::\{i3}\.'\{p7}\{i0}\:'\'.\{p6}\' \{p0}\::\::\::\{i6}\ '\:.\{i7}\':\{p7}\::\::\{i0}\ :\{p1}\{i5}\'.\..\.'\{p0}\''\' \{i7}\..\.'\{i6}\.:\{i0}\::\::\{p6}\ :\{p7}\. \{i7}\::\::\::\::\::\{p0}\{i3}\ '\{p2}\.'\{p0}\{i7}\.:\{p7}\::\{i0}\.'\{p6}\ :\{p0}\::\::\::\::\{i6}\ '\:.\{i7}\''\''\{i6}\.:\''\':\{i7}\ :\{p7}\::\::\{p0}\:'\{i6}\.:\' \{i0}\::\::\{i6}\''\:.\{i7}\''\':\{p7}\::\::\::\{p0}\{i6}\ :\{i7}\ :\{p7}\::\{p0}\''\{i6}\.:\{i0}\::\::\::\::\::\::\{i6}\ '\''\''\' \{i0}\::\{i6}\ :\{i7}\ '\''\' \{i6}\.:\' \{i0}\::\::\::\::\{i6}\ '\''\:.\..\..\..\:'\:.\..\:'\' \{i0}\::\::\::\::\::\::\::\::\::\::\::\::\{i6}\''\''\''\{i7}\..\..\{i0}\::\{p7}\:'\{p0}\::\::\{p7}\':\{p0}\::\{p7}\''\{p0}\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\{p7}\ :\{p0}\::\::\{i7}\ :\'.\{i0}\::\{p7}\ :\{p0}\::\{p7}\ :\.'\{p0}\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\{i7}\:'\' \{i0}\::\{i7}\ :\{i0}\::\{p7}\.'\ :\{p0}\::\{p7}\ :\: \{p0}\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\{p7}\ '\''\{p0}\::\{i7}\ :\{i0}\::\::\{p7}\ :\{p0}\::\{p7}\ '\.:\{p0}\::\::\::\::\::\::\::\::\::\::"
	if score>hiscore then
		let hiscore=score
		print at 22,6;"NEW HI-SCORE ";flash 1;hiscore;
	end if
	let laststage=0
	pause 200
	go to inicio
end If

if stage<>14 then
	let bonus = bonuslist(stage)
	pintanivel(stage)
else
	let enemigo1x=0:let enemigo1y=0
	let enemigo2x=0:let enemigo2y=0
	let lenemigo1x=0 :let lenemigo1y=0
	let lenemigo2x=0 :let lenemigo2y=0

	let atrapado1=0: let atrapado2=0
	let contador1=1: let contador2=1
	border 0
	paper 0
	ink 7
	let bonus=700
	cls
	rem dibujo paredes
	for n=0 to 18
		print at n,0;ink 4;pared$
		print at n,31;ink 4;pared$
		PRINT AT n,15;ink 4;pared$;pared$
	next n
	Print at 1,15;ink 7;paper 4;"EX"
	print at 2,15;ink 7;paper 4;"IT"

	rem dibuja suelo y techo
	for n=0 to 31
		print at 0,n;ink 4;pared$
		print at 19,n;ink 4;pared$
	next n
	rem dibuja marcador
			
	print at 20,3; ink 7;"STAGE ";ink 3;stage;
	print at 20,12; ink 7;"LIFES ";ink 3;life;
	print at 20,20; ink 7;"SCORE ";ink 3;score;
	print at 21,10; ink 7;"HI-SCORE ";ink 3;hiscore;
	
	rem muros
	INK 5
	Print at 3,6;muro$;muro$; at 4,6;muro$;muro$; at 5,6;muro$;muro$; 
	Print at 3,22;muro$;muro$; at 4,22;muro$;muro$; at 5,22;muro$;muro$
	print at 5,3;muro$; at 5,27;muro$
	
	print at 6,7;muro$; at 6,23;muro$; at 6,4;"\A"; at 6,27;"\A"
	print at 7,4;"\A"; at 7,7;muro$;muro$;"\A"; at 7,20;muro$;muro$;"\A"; at 7,27;"\A"
	print at 8,4;muro$;muro$;"\A"; at 8,11;"\A"; at 8,20;"\A"; at 8,23;muro$;muro$;"\A"
	print at 9,7;muro$; at 9,11;"\A"; at 9,20;"\A"; at 9,23;muro$
	print at 11,23;muro$; at 12,23;muro$
	print at 10,7;muro$; at 10,23;muro$; at 10,11;muro$; at 10,19;muro$
	print at 13,5;muro$;muro$;muro$; at 13,21;muro$;muro$;muro$
	print at 14,5;"\A"; at 14,10;"\A"; at 14,21;"\A"; at 14,26;"\A"
	print at 15,5;"\A"; at 15,10;"\A"; at 15,21;"\A"; at 15,26;"\A"
	print at 16,3;"\A\A\A"; at 16,10;"\A\A\A"; at 16,19;"\A\A\A"; at 16,26;"\A\A\A"

	rem enemigos 
	rem (-1 para no poner enemigo, sentido1 y sentido2 para los aleatorios y ldireccion1 y ldireccion2 para los lineales)
	Let sentido1=0
	let sentido2=-1
	
	let enemigo1x=1:let enemigo1y=7
	
	Print ink 3;at enemigo1x,enemigo1y;alea1$; at enemigo1x+1,enemigo1y;alea2$
	
		rem si ld=0 se mueve en horizontal si ld=1 se mueve en vertical
		rem ls=1 o -1 para derecha o izquierda y para bajar o subir 
	let ldireccion1=-1
	let ldireccion2=1

	let lenemigo2x=11 :let lenemigo2y=17: let lsentido2=1
	
	Print ink 3;at lenemigo2x,lenemigo2y;lin1$; at lenemigo2x+1,lenemigo2y;lin2$

end if


rem **********************************************************
rem ****** final de fase, comienzo del loop ******************
rem **********************************************************

REM bucle principal
buclePrincipal:

let fr=1-fr
if fr=0 then
	let alea2$="\E\F"
	let lin1$="\I\J"
Else
	let alea2$="\G\H"
	let lin1$="\M\N"
end If

rem comprobar final
if (attr(jugador1x,jugador1y+2)=39 and attr(jugador1x+1,jugador1y+2)=39) and (attr(jugador2x,jugador2y-1)=39 and attr(jugador2x+1,jugador2y-1)=39) then
	let score=score+100+10*stage
	print at jugador1x,jugador1y;ink 7;"  "
	Print At jugador1x+1,jugador1y;ink 7;"  "

	print at jugador2x,jugador2y;ink 7;"  "
	Print At jugador2x+1,jugador2y;ink 7;"  "
	
	let jugador1x=17: let jugador1y=1
	let jugador2x=17: let jugador2y=29
	cls 
	
	print at 7,12;"Score ";score
	print at 10,12;"Bonus ";bonus
	for i=bonus to 0 step -1
		print at 5,10;"Stage ";stage;" Clear"
		let score=score+1
		print at 7,18;score;"      "
		print at 10,18;i;"      "
		beep 0.01,0.08*bonus
	next i
	let stage=stage+1
	HALTLOOP(50*2)
	pause 0
	go to fases
end If

rem dibuja jugadores
if fp=0 then
	LET b$="\R\S"
Else
	LET b$="\T\U"
end If

if atrapado1=1 then
	print over 1; at jugador1x,jugador1y;ink 1;a$
	Print over 1; At jugador1x+1,jugador1y;ink 1;b$
	beep 0.05,-20
else:
	print at jugador1x,jugador1y;ink 1;a$
	Print At jugador1x+1,jugador1y;ink 1;b$
end If

if atrapado2=1 then
	print over 1; at jugador2x,jugador2y;ink 2;c$
	Print over 1; at jugador2x+1,jugador2y;ink 2;b$
	beep 0.05,-20
else:
	print at jugador2x,jugador2y;ink 2;c$
	Print At jugador2x+1,jugador2y;ink 2;b$
end If

let contador1=contador1+4
let contador2=contador2+4

if contador1>=99 and atrapado1=1 then
	let contador1=0
	let atrapado1=0
end If
if contador2>=99 and atrapado2=1 then
	let contador2=0
	let atrapado2=0
end If
pause 3
let bonus=bonus-5
if bonus<=0 then
	let bonus=0
End If

rem *****************************************************************
rem ****************** mueve enemigos ***************************
rem *****************************************************************

rem mover enemigos complejos
if sentido1=0 then
	direccion1=(rnd*4)+1
	sentido1=(rnd*8)+1
elseif sentido1>0 then
	sentido1=sentido1-1
	
	if direccion1=1 then
		rem derecha
		if attr(enemigo1x,enemigo1y+2)=1 or attr(enemigo1x+1,enemigo1y+2)=1 or attr(enemigo1x,enemigo1y+2)=2 or attr(enemigo1x+1,enemigo1y+2)=2 then
			go to colision
		end if
		if attr(enemigo1x,enemigo1y+2)=1 or attr(enemigo1x+1,enemigo1y+2)=1 or attr(enemigo1x,enemigo1y+2)=2 or attr(enemigo1x+1,enemigo1y+2)=2 then
			go to colision
		end if
		if (ATTR(enemigo1x,enemigo1y+2)=7 and ATTR(enemigo1x+1,enemigo1y+2)=7) or (ATTR(enemigo1x,enemigo1y+2)=3 and ATTR(enemigo1x+1,enemigo1y+2)=3) or (ATTR(enemigo1x,enemigo1y+2)=1 and ATTR(enemigo1x+1,enemigo1y+2)=1) or (ATTR(enemigo1x,enemigo1y+2)=2 and ATTR(enemigo1x+1,enemigo1y+2)=2)then
			print at enemigo1x,enemigo1y;ink 7;"  "
			Print At enemigo1x+1,enemigo1y;ink 7;"  "
			let enemigo1y=enemigo1y+1
			else:
				let sentido1=0
			end If
	elseIf direccion1=2 then
		rem izquierda
		if attr(enemigo1x,enemigo1y-1)=1 or attr(enemigo1x+1,enemigo1y-1)=1 or attr(enemigo1x,enemigo1y-1)=2 or attr(enemigo1x+1,enemigo1y-1)=2 then
			go to colision
		end If
		if attr(enemigo1x,enemigo1y-1)=1 or attr(enemigo1x+1,enemigo1y-1)=1 or attr(enemigo1x,enemigo1y-1)=2 or attr(enemigo1x+1,enemigo1y-1)=2 then
			go to colision
		end If
		if (ATTR(enemigo1x,enemigo1y-1)=7 and ATTR(enemigo1x+1,enemigo1y-1)=7) or (ATTR(enemigo1x,enemigo1y-1)=3 and ATTR(enemigo1x+1,enemigo1y-1)=3) or (ATTR(enemigo1x,enemigo1y-1)=1 and ATTR(enemigo1x+1,enemigo1y-1)=1) or (ATTR(enemigo1x,enemigo1y-1)=2 and ATTR(enemigo1x+1,enemigo1y-1)=2) then
			print at enemigo1x,enemigo1y;ink 7;"  "
			Print At enemigo1x+1,enemigo1y;ink 7;"  "
			let enemigo1y=enemigo1y-1
		else:
			let sentido1=0
		end If
	elseIf direccion1=3 then
		rem arriba
		if attr(enemigo1x+1,enemigo1y)=1 or attr(enemigo1x+1,enemigo1y+1)=1 or attr(enemigo1x+1,enemigo1y)=2 or attr(enemigo1x+1,enemigo1y+1)=2 then
			go to colision
		end If
		if attr(enemigo1x+1,enemigo1y)=1 or attr(enemigo1x+1,enemigo1y+1)=1 or attr(enemigo1x+1,enemigo1y)=2 or attr(enemigo1x+1,enemigo1y+1)=2 then
			go to colision
		end If
		if (ATTR(enemigo1x-1,enemigo1y)=7 and ATTR(enemigo1x-1,enemigo1y+1)=7) or (ATTR(enemigo1x-1,enemigo1y)=3 and ATTR(enemigo1x-1,enemigo1y+1)=3) or (ATTR(enemigo1x-1,enemigo1y)=1 and ATTR(enemigo1x-1,enemigo1y+1)=1) or (ATTR(enemigo1x-1,enemigo1y)=2 and ATTR(enemigo1x-1,enemigo1y+1)=2)then
			print at enemigo1x,enemigo1y;ink 7;"  "
			Print At enemigo1x+1,enemigo1y;ink 7;"  "
			let enemigo1x=enemigo1x-1
		else:
			let sentido1=0
		end If
	elseIf direccion1=4 then
		rem abajo
		if attr(enemigo1x-2,enemigo1y)=1 or attr(enemigo1x-2,enemigo1y+1)=1 or attr(enemigo1x-2,enemigo1y)=2 or attr(enemigo1x-2,enemigo1y+1)=2  then
			go to colision
		end If
		if attr(enemigo1x-2,enemigo1y)=1 or attr(enemigo1x-2,enemigo1y+1)=1 or attr(enemigo1x-2,enemigo1y)=2 or attr(enemigo1x-2,enemigo1y+1)=2  then
			go to colision
		end If
		if (ATTR(enemigo1x+2,enemigo1y)=7 and ATTR(enemigo1x+2,enemigo1y+1)=7) or (ATTR(enemigo1x+2,enemigo1y)=3 and ATTR(enemigo1x+2,enemigo1y+1)=3) or (ATTR(enemigo1x+2,enemigo1y)=1 and ATTR(enemigo1x+2,enemigo1y+1)=1) or  (ATTR(enemigo1x+2,enemigo1y)=2 and ATTR(enemigo1x+2,enemigo1y+1)=2) then
			print at enemigo1x,enemigo1y;ink 7;"  "
			Print At enemigo1x+1,enemigo1y;ink 7;"  "
			let enemigo1x=enemigo1x+1
		else:
			let sentido1=0
		end If
	end If
end If

if sentido1>=0 then
	Print ink 3;at enemigo1x,enemigo1y;alea1$; at enemigo1x+1,enemigo1y;alea2$
end If

if sentido2=0 then
	direccion2=(rnd*4)+1
	sentido2=(rnd*8)+1
elseif sentido2>0 then
	sentido2=sentido2-1
	
	if direccion2=1 then
		rem derecha
		if  attr(enemigo2x,enemigo2y+2)=1 or attr(enemigo2x+1,enemigo2y+2)=1 or attr(enemigo2x,enemigo2y+2)=2 or attr(enemigo2x+1,enemigo2y+2)=2 then
			go to colision
		end if
		if  attr(enemigo2x,enemigo2y+2)=1 or attr(enemigo2x+1,enemigo2y+2)=1 or attr(enemigo2x,enemigo2y+2)=2 or attr(enemigo2x+1,enemigo2y+2)=2 then
			go to colision
		end if
		if (ATTR(enemigo2x,enemigo2y+2)=7 and ATTR(enemigo2x+1,enemigo2y+2)=7) or (ATTR(enemigo2x,enemigo2y+2)=3 and ATTR(enemigo2x+1,enemigo2y+2)=3) or (ATTR(enemigo2x,enemigo2y+2)=1 and ATTR(enemigo2x+1,enemigo2y+2)=1) or (ATTR(enemigo2x,enemigo2y+2)=2 and ATTR(enemigo2x+1,enemigo2y+2)=2)then
			print at enemigo2x,enemigo2y;ink 7;"  "
			Print At enemigo2x+1,enemigo2y;ink 7;"  "
			let enemigo2y=enemigo2y+1
		else:
			let sentido2=0
		end If
	elseIf direccion2=2 then
		rem izquierda
		if attr(enemigo2x,enemigo2y-1)=1 or attr(enemigo2x+1,enemigo2y-1)=1 or attr(enemigo2x,enemigo2y-1)=2 or attr(enemigo2x+1,enemigo2y-1)=2 then
			go to colision
		end If
		if attr(enemigo2x,enemigo2y-1)=1 or attr(enemigo2x+1,enemigo2y-1)=1 or attr(enemigo2x,enemigo2y-1)=2 or attr(enemigo2x+1,enemigo2y-1)=2 then
			go to colision
		end If
		if (ATTR(enemigo2x,enemigo2y-1)=7 and ATTR(enemigo2x+1,enemigo2y-1)=7) or (ATTR(enemigo2x,enemigo2y-1)=3 and ATTR(enemigo2x+1,enemigo2y-1)=3) or (ATTR(enemigo2x,enemigo2y-1)=1 and ATTR(enemigo2x+1,enemigo2y-1)=2) or (ATTR(enemigo2x,enemigo2y-1)=2 and ATTR(enemigo2x+1,enemigo2y-1)=2) then
			print at enemigo2x,enemigo2y;ink 7;"  "
			Print At enemigo2x+1,enemigo2y;ink 7;"  "
			let enemigo2y=enemigo2y-1
		else:
			let sentido2=0
		end If
	elseIf direccion2=3 then
		rem arriba
		if attr(enemigo2x+1,enemigo2y)=1 or attr(enemigo2x+1,enemigo2y+1)=1 or attr(enemigo2x+1,enemigo2y)=2 or attr(enemigo2x+1,enemigo2y+1)=2 then
		 go to colision
		end If
		if attr(enemigo2x+1,enemigo2y)=1 or attr(enemigo2x+1,enemigo2y+1)=1 or attr(enemigo2x+1,enemigo2y)=2 or attr(enemigo2x+1,enemigo2y+1)=2 then
		 go to colision
		end If
		if (ATTR(enemigo2x-1,enemigo2y)=7 and ATTR(enemigo2x-1,enemigo2y+1)=7) or (ATTR(enemigo2x-1,enemigo2y)=3 and ATTR(enemigo2x-1,enemigo2y+1)=3) or (ATTR(enemigo2x-1,enemigo2y)=1 and ATTR(enemigo2x-1,enemigo2y+1)=1) or (ATTR(enemigo2x-1,enemigo2y)=2 and ATTR(enemigo2x-1,enemigo2y+1)=3) then
			print at enemigo2x,enemigo2y;ink 7;"  "
			Print At enemigo2x+1,enemigo2y;ink 7;"  "
			let enemigo2x=enemigo2x-1
		else:
			let sentido2=0
		end If
	Elseif direccion2=4 then
		rem abajo
		if attr(enemigo2x-2,enemigo2y)=1 or attr(enemigo2x-2,enemigo2y+1)=1 or attr(enemigo2x-2,enemigo2y)=2 or attr(enemigo2x-2,enemigo2y+1)=2  then
			go to colision
		end If
		if attr(enemigo2x-2,enemigo2y)=1 or attr(enemigo2x-2,enemigo2y+1)=1 or attr(enemigo2x-2,enemigo2y)=2 or attr(enemigo2x-2,enemigo2y+1)=2  then
			go to colision
		end If
		if (ATTR(enemigo2x+2,enemigo2y)=7 and ATTR(enemigo2x+2,enemigo2y+1)=7) or  (ATTR(enemigo2x+2,enemigo2y)=3 and ATTR(enemigo2x+2,enemigo2y+1)=3) or (ATTR(enemigo2x+2,enemigo2y)=1 and ATTR(enemigo2x+2,enemigo2y+1)=1) or  (ATTR(enemigo2x+2,enemigo2y)=2 and ATTR(enemigo2x+2,enemigo2y+1)=2) then
			print at enemigo2x,enemigo2y;ink 7;"  "
			Print At enemigo2x+1,enemigo2y;ink 7;"  "
			let enemigo2x=enemigo2x+1
		else:
			let sentido2=0
		end If
	end If
end If

if sentido2>=0 then
	Print ink 3;at enemigo2x,enemigo2y;alea1$; at enemigo2x+1,enemigo2y;alea2$
end If

rem ******************** mover enemigos simples ****************

	if ldireccion1=0 then
			if lsentido1=-1 then
				If ATTR(lenemigo1x,lenemigo1y+2)=2 or ATTR(lenemigo1x+1,lenemigo1y+2)=2 or ATTR(lenemigo1x,lenemigo1y+2)=1 or ATTR(lenemigo1x+1,lenemigo1y+2)=1 Then
					go to colision
				end If
			else
				If ATTR(lenemigo1x,lenemigo1y-1)=2 or ATTR(lenemigo1x+1,lenemigo1y-1)=2 or ATTR(lenemigo1x,lenemigo1y-1)=1 or ATTR(lenemigo1x+1,lenemigo1y-1)=1 Then
					go to colision
				end If
			end if
			print at lenemigo1x,lenemigo1y;ink 7;"  "
			Print At lenemigo1x+1,lenemigo1y;ink 7;"  "
			let lenemigo1y=lenemigo1y+lsentido1
			if (ATTR(lenemigo1x,lenemigo1y+2)<>7 or ATTR(lenemigo1x+1,lenemigo1y+2)<>7) or (ATTR(lenemigo1x,lenemigo1y-1)<>7 or ATTR(lenemigo1x+1,lenemigo1y-1)<>7) then	
				let lsentido1=-lsentido1
			end If
	elseif ldireccion1=1 then
			if lsentido1=-1 then
				if ATTR(lenemigo1x+2,lenemigo1y)=2 or ATTR(lenemigo1x+2,lenemigo1y+1)=2 or ATTR(lenemigo1x+2,lenemigo1y)=1 or ATTR(lenemigo1x+2,lenemigo1y+1)=1 then
					go to colision
				end If
			else
				if ATTR(lenemigo1x-1,lenemigo1y)=2 or ATTR(lenemigo1x-1,lenemigo1y+1)=2 or ATTR(lenemigo1x-1,lenemigo1y)=1 or ATTR(lenemigo1x-1,lenemigo1y+1)=1 Then
					go to colision
				end If
			end if
			print at lenemigo1x,lenemigo1y;ink 7;"  "
			Print At lenemigo1x+1,lenemigo1y;ink 7;"  "
			let lenemigo1x=lenemigo1x+lsentido1
			if (ATTR(lenemigo1x-1,lenemigo1y)<>7 or ATTR(lenemigo1x-1,lenemigo1y+1)<>7) or (ATTR(lenemigo1x+2,lenemigo1y)<>7 or ATTR(lenemigo1x+2,lenemigo1y+1)<>7) then
				let lsentido1= -lsentido1
			end If
	end If
if ldireccion1>=0 then 
	Print ink 3;at lenemigo1x,lenemigo1y;lin1$; at lenemigo1x+1,lenemigo1y;lin2$
end If

	if ldireccion2=0 then
		if lsentido2=-1 then
			If ATTR(lenemigo2x,lenemigo2y+2)=2 or ATTR(lenemigo2x+1,lenemigo2y+2)=2 or ATTR(lenemigo2x,lenemigo2y+2)=1 or ATTR(lenemigo2x+1,lenemigo2y+2)=1 then
				go to colision
			end If
		else
			If ATTR(lenemigo2x,lenemigo2y-1)=2 or ATTR(lenemigo2x+1,lenemigo2y-1)=2 or ATTR(lenemigo2x,lenemigo2y-1)=1 or ATTR(lenemigo2x+1,lenemigo2y-1)=1 Then
				go to colision
			end If
		end if
		print at lenemigo2x,lenemigo2y;ink 7;"  "
		Print At lenemigo2x+1,lenemigo2y;ink 7;"  "
		let lenemigo2y=lenemigo2y+lsentido2
		if (ATTR(lenemigo2x,lenemigo2y+2)<>7 or ATTR(lenemigo2x+1,lenemigo2y+2)<>7) or (ATTR(lenemigo2x,lenemigo2y-1)<>7 or ATTR(lenemigo2x+1,lenemigo2y-1)<>7) then	
			let lsentido2=-lsentido2
		end If
	elseif ldireccion2=1 then
		if lsentido2=-1 then
			If ATTR(lenemigo2x+2,lenemigo2y)=2 or ATTR(lenemigo2x+2,lenemigo2y+1)=2 or ATTR(lenemigo2x+2,lenemigo2y)=1 or ATTR(lenemigo2x+2,lenemigo2y+1)=1 Then
				go to colision
			end If
		else
			if ATTR(lenemigo2x-1,lenemigo2y)=2 or ATTR(lenemigo2x-1,lenemigo2y+1)=2 or ATTR(lenemigo2x-1,lenemigo2y)=1 or ATTR(lenemigo2x-1,lenemigo2y+1)=1 Then
				go to colision
			end If
		end if
		
		print at lenemigo2x,lenemigo2y;ink 7;"  "
		Print At lenemigo2x+1,lenemigo2y;ink 7;"  "
		let lenemigo2x=lenemigo2x+lsentido2
		if (ATTR(lenemigo2x-1,lenemigo2y)<>7 or ATTR(lenemigo2x-1,lenemigo2y+1)<>7) or (ATTR(lenemigo2x+2,lenemigo2y)<>7 or ATTR(lenemigo2x+2,lenemigo2y+1)<>7) then
			let lsentido2= -lsentido2
		end If
	end If
if ldireccion2>=0 then 	
	Print ink 3;at lenemigo2x,lenemigo2y;lin1$; at lenemigo2x+1,lenemigo2y;lin2$
end if

rem _________________________________________________________
rem _______________ mover personaje ________________________
rem _________________________________________________________

rem mueve a la derecha al pulsar p
IF IN 57342=254 OR IN 57342=190 THEN 
	let fp=1-fp
	if ATTR(jugador1x,jugador1y+2)=3 or ATTR(jugador1x+1,jugador1y+2)=3 then
		go to colision
	end if
	if ATTR(jugador1x,jugador1y+2)=7 and ATTR(jugador1x+1,jugador1y+2)=7 and atrapado1=0 then
		print at jugador1x,jugador1y;ink 7;"  "
		Print At jugador1x+1,jugador1y;ink 7;"  "
		let jugador1y=jugador1y+1		
	end If
	if ATTR(jugador2x,jugador2y-1)=3 or ATTR(jugador2x+1,jugador2y-1)=3 then
		go to colision
	end If
	if ATTR(jugador2x,jugador2y-1)=7 and ATTR(jugador2x+1,jugador2y-1)=7 and atrapado2=0 then
		print at jugador2x,jugador2y;ink 7;"  "
		Print At jugador2x+1,jugador2y;ink 7;"  "
		let jugador2y=jugador2y-1
	end If
		if ATTR(jugador1x,jugador1y+2)=6 or ATTR(jugador1x+1,jugador1y+2)=6 and atrapado1=0 then
		print at jugador1x,jugador1y;ink 7;"  "
		Print At jugador1x+1,jugador1y;ink 7;"  "
		let jugador1y=jugador1y+1
		let atrapado1=1
		let contador1=0
	end If
	if ATTR(jugador2x,jugador2y-1)=6 or ATTR(jugador2x+1,jugador2y-1)=6 and atrapado2=0 then
		print at jugador2x,jugador2y;ink 7;"  "
		Print At jugador2x+1,jugador2y;ink 7;"  "
		let jugador2y=jugador2y-1
		let atrapado2=1
		let contador2=0
	end If
	go to buclePrincipal
end If

rem mueve a la izquierda al pulsar o
IF IN 57342=253 OR IN 57342=189 THEN 
	let fp=1-fp
	if ATTR(jugador1x,jugador1y-1)=3 or ATTR(jugador1x+1,jugador1y-1)=3 then
			go to colision
	end If
	if ATTR(jugador1x,jugador1y-1)=7 and ATTR(jugador1x+1,jugador1y-1)=7 and atrapado1=0 then
		print at jugador1x,jugador1y;ink 7;"  "
		Print At jugador1x+1,jugador1y;ink 7;"  "
		let jugador1y=jugador1y-1		
	end If
		if ATTR(jugador2x,jugador2y+2)=3 or ATTR(jugador2x+1,jugador2y+2)=3 then
		go to colision
	end If
	if ATTR(jugador2x,jugador2y+2)=7 and ATTR(jugador2x+1,jugador2y+2)=7 and atrapado2=0 then
		print at jugador2x,jugador2y;ink 7;"  "
		Print At jugador2x+1,jugador2y;ink 7;"  "
		let jugador2y=jugador2y+1
	end If
		if ATTR(jugador1x,jugador1y-1)=6 or ATTR(jugador1x+1,jugador1y-1)=6 and atrapado1=0 then
		print at jugador1x,jugador1y;ink 7;"  "
		Print At jugador1x+1,jugador1y;ink 7;"  "
		let jugador1y=jugador1y-1
		let atrapado1=1
		let contador1=0
	end If
	if ATTR(jugador2x,jugador2y+2)=6 or ATTR(jugador2x+1,jugador2y+2)=6 and atrapado2=0 then
		print at jugador2x,jugador2y;ink 7;"  "
		Print At jugador2x+1,jugador2y;ink 7;"  "
		let jugador2y=jugador2y+1
		let atrapado2=1
		let contador2=0
	end If
	go to buclePrincipal
end If

rem mueve arriba al pulsar q
IF IN 64510=254 OR IN 64510=190 THEN
	let fp=1-fp
	if ATTR(jugador1x-1,jugador1y+1)=3 or ATTR(jugador1x-1,jugador1y)=3  then
		go to colision
	End if 
	if ATTR(jugador1x-1,jugador1y+1)=7 and ATTR(jugador1x-1,jugador1y)=7 and atrapado1=0 then
		print at jugador1x,jugador1y;ink 7;"  "
		Print At jugador1x+1,jugador1y;ink 7;"  "
		let jugador1x=jugador1x-1
	End if
		if ATTR(jugador2x-1,jugador2y+1)=3 or ATTR(jugador2x-1,jugador2y)=3 then
		go to colision
	End if
	if ATTR(jugador2x-1,jugador2y+1)=7 and ATTR(jugador2x-1,jugador2y)=7 and atrapado2=0 then
		print at jugador2x,jugador2y;ink 7;"  "
		Print At jugador2x+1,jugador2y;ink 7;"  "
		let jugador2x=jugador2x-1
	End if
	if ATTR(jugador1x-1,jugador1y+1)=6 or ATTR(jugador1x-1,jugador1y)=6 and atrapado1=0 then
		print at jugador1x,jugador1y;ink 7;"  "
		Print At jugador1x+1,jugador1y;ink 7;"  "
		let jugador1x=jugador1x-1
		let atrapado1=1
		let contador1=0
	End if
	if ATTR(jugador2x-1,jugador2y+1)=6 or ATTR(jugador2x-1,jugador2y)=6 and atrapado2=0 then
		print at jugador2x,jugador2y;ink 7;"  "
		Print At jugador2x+1,jugador2y;ink 7;"  "
		let jugador2x=jugador2x-1
		let atrapado2=1
		let contador2=0
	End if
		go to buclePrincipal
end If

rem mueve abajo al pulsar a
IF IN 65022=254 OR IN 65022=190 THEN 
	let fp=1-fp
	if ATTR(jugador1x+2,jugador1y+1)=3 or ATTR(jugador1x+2,jugador1y)=3 then
		go to colision
	end If
	if ATTR(jugador1x+2,jugador1y+1)=7 and ATTR(jugador1x+2,jugador1y)=7 and atrapado1=0 then
		print at jugador1x,jugador1y;ink 7;"  "
		Print At jugador1x+1,jugador1y;ink 7;"  "
		let jugador1x=jugador1x+1
	end If
	if ATTR(jugador2x+2,jugador2y+1)=3 or ATTR(jugador2x+2,jugador2y)=3 then
		go to colision
	end If
	if ATTR(jugador2x+2,jugador2y+1)=7 and ATTR(jugador2x+2,jugador2y)=7 and atrapado2=0 then
		print at jugador2x,jugador2y;ink 7;"  "
		Print At jugador2x+1,jugador2y;ink 7;"  "
		let jugador2x=jugador2x+1
	end If
	if ATTR(jugador1x+2,jugador1y+1)=6 or ATTR(jugador1x+2,jugador1y)=6 and atrapado1=0 then
		print at jugador1x,jugador1y;ink 7;"  "
		Print At jugador1x+1,jugador1y;ink 7;"  "
		let jugador1x=jugador1x+1
		let atrapado1=1
		let contador1=0
	end If
	if ATTR(jugador2x+2,jugador2y+1)=6 or ATTR(jugador2x+2,jugador2y)=6 and atrapado2=0 then
		print at jugador2x,jugador2y;ink 7;"  "
		Print At jugador2x+1,jugador2y;ink 7;"  "
		let jugador2x=jugador2x+1
		let atrapado2=1
		let contador2=0
	end If
	go to buclePrincipal
end If

go to buclePrincipal        

rem colision
colision:
	beep 0.21,0
	beep 0.21,-10
	beep 0.21,-15
	beep 0.121,-30
	let life=life-1
	HALTLOOP(10)
	let jugador1x=17: let jugador1y=1
	let jugador2x=17: let jugador2y=29
	if life>=0 then
			go to fases   
	end If
	
if life<0 then
	cls
	print at 10,12;ink 6;"Game Over"
	let laststage=stage
	if score>hiscore then
		let hiscore=score
		print at 14,6;"NEW HI-SCORE ";flash 1;hiscore;
	end if
	HALTLOOP(50*2)
	print at 12,4; ink 6;"Push any key to re-start"
	pause 0
	go to inicio
end If 

rem *-*-*-*-*-*-*-*-*- subrutinas *-*-*-*-*-*-*-*-*-*-*-*-

SUB FASTCALL HALTLOOP(wait AS UBYTE)
ASM
PROC
LOCAL loop
        LD B, A
loop:   HALT        ; wait for interrupt
        DJNZ loop
ENDP
END ASM
END SUB


SUB pintanivel( nivel as BYTE )

	let enemigo1x=0:let enemigo1y=0
	let enemigo2x=0:let enemigo2y=0
	let lenemigo1x=0: let lenemigo1y=0
	let lenemigo2x=0: let lenemigo2y=0
	Let sentido1=-1: let sentido2=-1
	let ldireccion1=-1: let ldireccion2=-1
	
	let atrapado1=0: let atrapado2=0
	let contador1=1: let contador2=1
	border 0
	paper 0
	ink 7
	cls
	rem dibujo paredes
	for n=0 to 18
		print at n,0;ink 4;pared$;
		print at n,31;ink 4;pared$;
		PRINT AT n,15;ink 4;pared$;pared$;
	next n
	Print at 1,15;ink 7;paper 4;"EX"
	print at 2,15;ink 7;paper 4;"IT"

	rem dibuja suelo y techo
	for n=0 to 31
		print at 0,n;ink 4;pared$;
		print at 19,n;ink 4;pared$;
	next n
	rem dibuja marcador
	
	print at 20,3; ink 7;"STAGE ";ink 3;stage;
	print at 20,12; ink 7;"LIFES ";ink 3;life;
	print at 20,20; ink 7;"SCORE ";ink 3;score;
	print at 21,10; ink 7;"HI-SCORE ";ink 3;hiscore;

    for x=0 TO 8
        	for y=0 TO 14
        		if pantallas(nivel, x, y)>0 then
        			if pantallas(nivel, x, y)=1 then
						INK 5
						Print at 2*x+1,2*y+1;muro$
						Print at 2*x+2,2*y+1;muro$
					elseif pantallas(nivel, x, y)=2 then
						INK 6
						print at 2*x+1,2*y+1;"\B"
					elseif pantallas(nivel, x, y)=3 then
						let ldireccion1=1: let lsentido1=1
						rem si ld=0 se mueve en horizontal si ld=1 se mueve en vertical
						rem ls=1 o -1 para derecha o izquierda y para bajar o subir 
						let lenemigo1x=2*x+1 :let lenemigo1y=2*y+1
						Print ink 3;at lenemigo1x,lenemigo1y;lin1$; at lenemigo1x+1,lenemigo1y;lin2$
					elseif pantallas(nivel, x, y)=4 then
						let ldireccion1=1: let lsentido1=-1
						rem si ld=0 se mueve en horizontal si ld=1 se mueve en vertical
						rem ls=1 o -1 para derecha o izquierda y para bajar o subir 
						let lenemigo1x=2*x+1 :let lenemigo1y=2*y+1
						Print ink 3;at lenemigo1x,lenemigo1y;lin1$; at lenemigo1x+1,lenemigo1y;lin2$
					elseif pantallas(nivel, x, y)=5 then
						let ldireccion1=0: let lsentido1=1
						rem si ld=0 se mueve en horizontal si ld=1 se mueve en vertical
						rem ls=1 o -1 para derecha o izquierda y para bajar o subir 
						let lenemigo1x=2*x+1 :let lenemigo1y=2*y+1
						Print ink 3;at lenemigo1x,lenemigo1y;lin1$; at lenemigo1x+1,lenemigo1y;lin2$
					elseif pantallas(nivel, x, y)=6 then
						let ldireccion1=0: let lsentido1=1
						rem si ld=0 se mueve en horizontal si ld=1 se mueve en vertical
						rem ls=1 o -1 para derecha o izquierda y para bajar o subir 
						let lenemigo1x=2*x+1 :let lenemigo1y=2*y+1
						Print ink 3;at lenemigo1x,lenemigo1y;lin1$; at lenemigo1x+1,lenemigo1y;lin2$
					elseif pantallas(nivel, x, y)=7 then
						rem (-1 para no poner enemigo, sentido1 y sentido2 para los aleatorios)
						let sentido1=0
						let enemigo1x=2*x+1:let enemigo1y=2*y+1
						Print ink 3;at enemigo1x,enemigo1y;alea1$; at enemigo1x+1,enemigo1y;alea2$
					elseif pantallas(nivel, x, y)=8 then
						let ldireccion2=1: let lsentido2=1
						rem si ld=0 se mueve en horizontal si ld=1 se mueve en vertical
						rem ls=1 o -1 para derecha o izquierda y para bajar o subir 
						let lenemigo2x=2*x+1 :let lenemigo2y=2*y+1
						Print ink 3;at lenemigo2x,lenemigo2y;lin1$; at lenemigo2x+1,lenemigo2y;lin2$
					elseif pantallas(nivel, x, y)=9 then
						let ldireccion2=1: let lsentido2=-1
						rem si ld=0 se mueve en horizontal si ld=1 se mueve en vertical
						rem ls=1 o -1 para derecha o izquierda y para bajar o subir 
						let lenemigo2x=2*x+1 :let lenemigo2y=2*y+1
						Print ink 3;at lenemigo2x,lenemigo2y;lin1$; at lenemigo2x+1,lenemigo2y;lin2$
					elseif pantallas(nivel, x, y)=10 then
						let ldireccion2=0: let lsentido2=1
						rem si ld=0 se mueve en horizontal si ld=1 se mueve en vertical
						rem ls=1 o -1 para derecha o izquierda y para bajar o subir 
						let lenemigo2x=2*x+1 :let lenemigo2y=2*y+1
						Print ink 3;at lenemigo2x,lenemigo2y;lin1$; at lenemigo2x+1,lenemigo2y;lin2$
					elseif pantallas(nivel, x, y)=11 then
						let ldireccion2=0: let lsentido2=-1
						rem si ld=0 se mueve en horizontal si ld=1 se mueve en vertical
						rem ls=1 o -1 para derecha o izquierda y para bajar o subir 
						let lenemigo2x=2*x+1 :let lenemigo2y=2*y+1
						Print ink 3;at lenemigo2x,lenemigo2y;lin1$; at lenemigo2x+1,lenemigo2y;lin2$
					elseif pantallas(nivel, x, y)=12 then
						rem (-1 para no poner enemigo, sentido1 y sentido2 para los aleatorios)
						let sentido2=0
						let enemigo2x=2*x+1:let enemigo2y=2*y+1
						Print ink 3;at enemigo2x,enemigo2y;alea1$; at enemigo2x+1,enemigo2y;alea2$
					end if
				end if
			next y
		next
END SUB