#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printAt(short x, short y, char[] text);

void printInit(void);
void printCircle(short numberSquare);
void clearCircle(short numberSquare);
void printCross(short numberSquare);

short numberSquareX (short numberSquare);
short numberSquareY (short numberSquare);

void cleanMsg(void);
void printMsgGameOver(void);
void printMsgFree(void);
void printMsgSelect(void);
void printMsgWait(void);
void resetGame(void);
short checkKey (short key);

int main(void)
{

	gen_tv_field_init(0);

  Init:

  printInit();
  printAt(7,25,"wait");
  printCross(5);
  gen_tv_field();
  short valueBox[9];
  short i = 0;

  for (i = 0; i < 9; ++i)
  {
    valueBox[i] = 0;
  }
  valueBox[4] = 1;

  short trap = 0;

  printMsgSelect();

  MARK1:
  gen_tv_field();
  int key = getk();

  //todo refactor
  if (checkKey(key) == 1) {

    if (valueBox[key-49] != 0){

      cleanMsg();
      printMsgFree();
      goto MARK1;

    } else {

      printCircle(key-48);
      valueBox[key-49] = 2;

      if (key == 49 || key == 51 || key == 55 || key == 57){
        trap = key;
      }
    }

  } else if(key!='\0'){

    cleanMsg();
    printMsgFree();
    goto MARK1;

  } else if(key =='\0'){

    goto MARK1;
  }


  printMsgWait();

  if (trap > 0) {
    printAt(14,22," look");
    printAt(15,22," behind");
    printAt(16,22," you, a");
    printAt(17,22," three");
    printAt(18,22," headed");
    printAt(19,22," monkey");

    printAt(21,22,"press any");
    printAt(22,22,"key to");
    printAt(23,22,"continue");

    gen_tv_field();

    fgetc_cons();        // wait for keypress
    cleanMsg();

    gen_tv_field();
    clearCircle(trap-48);

    valueBox[trap-49] = 0;

    switch (trap){
      case 49:
      case 55:
      trap = 52;
      break;

      case 51:
      case 57:
      trap = 54;
      break;
    }

    printCircle(trap-48);

    valueBox[trap-49] = 2;
  } else {
    trap = -1;
  }


  printCross(9);
  valueBox[8] = 1;
  
  printMsgSelect();

  gen_tv_field();

  MARK2:
  gen_tv_field();
  key = getk();
  
  //todo refactor  
  if (checkKey(key) == 1) {

    if (valueBox[key-49] != 0){

      cleanMsg();
      printMsgFree();
      goto MARK2;

    } else {

      printCircle(key-48);
      valueBox[key-49] = 2;

    }
    
  } else if(key !='\0'){

    cleanMsg();
    printMsgFree();
    goto MARK2;

  } else if(key =='\0'){

    goto MARK2;
  }

  printMsgWait();

  //check if zx80 win
  if (valueBox[0] == 0){
    printCross(1);
    resetGame();    
    goto Init;
  }

  if (valueBox[3] == 2 || valueBox[5] == 2) {

    printCross(7);
    valueBox[6] = 1;

  } else {

    printCross(3);
    valueBox[2] = 1;

  }

  printMsgSelect();
  gen_tv_field();
  
  MARK3:
  gen_tv_field();
  key = getk();

  gen_tv_field();
  
  //todo refactor 
  if (checkKey(key) == 1) {

    if (valueBox[key-49] != 0){

      cleanMsg();
      printMsgFree();
      goto MARK3;

    } else {

      printCircle(key-48);
      valueBox[key-49] = 2;

    }

  } else if(key!='\0'){

    cleanMsg();
    printMsgFree();
    goto MARK3;

  } else if(key =='\0'){

    goto MARK3;
  }
  
  printMsgWait();

  if (valueBox[6] == 1 && valueBox[8] == 1 && valueBox[7] == 0) {
    printCross(8);
    resetGame();    
    goto Init;
  } else if (valueBox[6] == 1 && valueBox[4] == 1 && valueBox[2] == 0) {
    printCross(3);
    resetGame();
    goto Init;
  } else if (valueBox[2] == 1 && valueBox[8] == 1 && valueBox[5] == 0) {
    printCross(6);
    resetGame();
    goto Init;
  } else if (valueBox[2] == 1 && valueBox[4] == 1 && valueBox[6] == 0) {
    printCross(7);
    resetGame();
    goto Init;
  }

  zx_cls();
}

void printAt(short x, short y, char[] text){
  zx_setcursorpos(x,y);
  printf(text);
  return;
}

void clearCircle(short numberSquare){
  int posX = numberSquareX (numberSquare);
  int posY = numberSquareY (numberSquare);
  printAt(posX + 1,posY,"      ");
  printAt(posX + 2,posY,"      ");
  printAt(posX + 3,posY,"      ");
  zx_setcursorpos(posX + 4,posY);
  printf("  %d   ",numberSquare);
  printAt(posX + 5,posY,"      ");
  printAt(posX + 6,posY,"      ");
}

void printCircle(short numberSquare){
  short posX = numberSquareX (numberSquare);
  short posY = numberSquareY (numberSquare);

  zx_setcursorpos(posX + 1,posY);
  zx_asciimode(0);
  printf("%c%c%c%c%c%c", 0, 135,131,131,134, 0);
  zx_setcursorpos(posX + 2,posY);
  printf("%c%c%c%c%c%c", 132, 0, 0, 0, 0, 133);
  zx_setcursorpos(posX + 3,posY);
  printf("%c%c%c%c%c%c", 2, 0, 0, 0, 0, 130);
  zx_setcursorpos(posX + 4,posY);
  printf("%c%c%c%c%c%c", 2, 0, 0, 0, 0, 130);
  zx_setcursorpos(posX + 5,posY);
  printf("%c%c%c%c%c%c", 134, 0, 0, 0, 0,135);
  zx_setcursorpos(posX + 6,posY);
  printf("%c%c%c%c%c%c", 0, 133,3,3,132,0);
  zx_asciimode(1);

}

short numberSquareX (short numberSquare) {
  short posX;
  switch (numberSquare){
    case 1:
    case 2:
    case 3:
    posX = -1;
    break;

    case 4:
    case 5:
    case 6:
    posX = 7;
    break;

    case 7:
    case 8:
    case 9:
    posX = 15;
    break;
  }
  return posX;
}

short numberSquareY (short numberSquare) {
  short posY;
  switch (numberSquare){
    case 1:
    case 4:
    case 7:
    posY = 0;
    break;

    case 2:
    case 5:
    case 8:
    posY = 8;
    break;

    case 3:
    case 6:
    case 9:
    posY = 16;
    break;
  }
  return posY;
}

void printCross(short numberSquare){
  short posX = numberSquareX (numberSquare);
  short posY = numberSquareY (numberSquare);

  zx_setcursorpos(posX + 1,posY);
  zx_asciimode(0);
  printf("%c%c%c%c%c%c", 128,0,0,0,0,128);
  zx_setcursorpos(posX + 2,posY);
  printf("%c%c%c%c%c%c", 0,128,0,0,128,0);
  zx_setcursorpos(posX + 3,posY);
  printf("%c%c%c%c%c%c",  0,0,136, 128,0,0);
  zx_setcursorpos(posX + 4,posY);
  printf("%c%c%c%c%c%c",  0,0,128, 136,0,0);
  zx_setcursorpos(posX + 5,posY);
  printf("%c%c%c%c%c%c", 0,128,0,0,128,0);
  zx_setcursorpos(posX + 6,posY);
  printf("%c%c%c%c%c%c", 128,0,0,0,0,128);
  zx_asciimode(1);

}

void printInit(){

  zx_cls();
  zx_setcursorpos(1,1);
  zx_asciimode(0);
  printf("%c%c%c%c%c%c%c%c%c%c%c", 9,9,9,9,9, 0, 137, 0,9,9,9);
  zx_setcursorpos(2,3);
  printf("%c%c%c%c%c%c%c", 9,0,0,0,0,0,9);
  zx_setcursorpos(3,3);
  printf("%c%c%c%c%c%c%c", 9,0,0,0,9, 0, 9);
  zx_setcursorpos(4,3);
  printf("%c%c%c%c%c%c%c%c%c", 9,0,0,0,9, 0, 9,9,9);

  zx_setcursorpos(6,9);
  printf("%c%c%c%c%c%c%c%c%c%c%c%c%c", 9,9,9,9,9, 0, 137,137,137, 0,9,9,9);
  zx_setcursorpos(7,11);
  printf("%c%c%c%c%c%c%c%c%c", 9,0,0,0, 137,0,137, 0, 9);
  zx_setcursorpos(8,11);
  printf("%c%c%c%c%c%c%c%c%c", 9,0,0,0, 137,137,137, 0, 9);
  zx_setcursorpos(9,11);
  printf("%c%c%c%c%c%c%c%c%c%c%c", 9,0,0,0, 137,0,137, 0, 9,9,9);

  zx_setcursorpos(11,0);
  printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 9,9,9,9,9, 0, 137,137,137,137, 0,9,11,11);
  zx_setcursorpos(12,2);
  printf("%c%c%c%c%c%c%c%c%c%c", 9,0,0,0, 137,0,0,137, 0, 9);
  zx_setcursorpos(12,2);
  printf("%c%c%c%c%c%c%c%c%c%c%c", 9,0,0,0, 137,0,0,137, 0, 9,9);
  zx_setcursorpos(13,2);
  printf("%c%c%c%c%c%c%c%c%c%c", 9,0,0,0, 137,0,0,137, 0, 9);
  zx_setcursorpos(14,2);
  printf("%c%c%c%c%c%c%c%c%c%c%c%c", 9,0,0, 0, 137,137,137,137, 0,9,9,9);

  zx_setcursorpos(12,16);
  printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 137,0,0,137,0, 9, 11,11, 0,9, 0,0,0,9);
  zx_setcursorpos(13,16);
  printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 137,137,137,137,0, 9, 9,9, 0,9, 0,0,0,9);
  zx_setcursorpos(14,16);
  printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 137,0,0,137,0, 9, 0,0, 0,9, 0,0,0,9);
  zx_setcursorpos(15,16);
  printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 137,0,0,137,0, 9, 9,9, 0,9, 9,9,0,9,9,9);
  zx_asciimode(1);

  printAt(17,0,"you know how to play tic-tac-toe but thanks to the zx80s ai you will never win. start playing the zx80, be careful that this game has surprises");
  printAt(23,4,"press any key to start");

  fgetc_cons();        // wait for keypress

  zx_cls();

  short i = 0;

  for (i = 0; i < 22; ++i)
  {
    zx_setcursorpos(7,i);
    zx_asciimode(0);
    printf("%c", 11);
    zx_setcursorpos(15,i); 
    printf("%c", 11);
    /*
    zx_setcursorpos(i,20);
    printf("%c", 9);
    */
    zx_asciimode(1);
    //zx_setcursorpos(i,0);
    //printf("%d",ir);
  }
  for (i = 0; i < 23; ++i)
  {
    zx_setcursorpos(i,6);
    zx_asciimode(0);
    printf("%c", 130);
    zx_setcursorpos(i,14);
    printf("%c", 130);
    zx_asciimode(1);
    //zx_setcursorpos(i,0);
    //printf("%d",i);
  }


  printAt(3,2, "1");
  printAt(3,10,"2");
  printAt(3,18,"3");

  printAt(11,2, "4");
  printAt(11,10,"5");
  printAt(11,18,"6");

  printAt(18,2, "7");
  printAt(18,10,"8");
  printAt(18,18,"9");

  printAt(0,23,"TicTacToe");
  printAt(1,25,"Hell");

  return;
}

void cleanMsg(){
  short i = 9;
  for (i = 9; i < 24; ++i){
    printAt(i, 22,"         ");
  }
  return;
}

void printMsgGameOver(){
    printAt(7,22,"          ");
    printAt(7,22,"you lose");
    printAt(9,22,"press any");
    printAt(10,22," key to ");
    printAt(11,22,"  start   ");    
    printAt(12,22,"         ");
  return;
}

void printMsgSelect(){
  printAt(7,22,"select you");  
  printAt(9,24,"please");  
  printAt(10,25,"press");  
  printAt(11,24,"from 1");  
  printAt(12,25,"to 9");  
}

void printMsgFree(){
  printAt(9,24,"please");  
  printAt(10,24,"select");  
  printAt(11,24,"free box");
  return;
}


void printMsgWait(){
  printAt(7,22,"          ");
  printAt(7,25,"wait");
  return;
}

void resetGame(){  
    printMsgGameOver();
    gen_tv_field();

    fgetc_cons();        // wait for keypress   
}

short checkKey (short key){
  if (key == 49 || key == 50 || key == 51 || 
    key == 52 || key == 53 || key == 54 || 
    key == 55 || key == 56 || key == 57){
    return 1;  
  }
  return 0;
}