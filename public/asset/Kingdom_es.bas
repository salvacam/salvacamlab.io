   5 REM "P"
  10 PRINT "   \ .\. \ .\. \..\ .\.  \..\ .\..\..\. \..\..\. \ .\..\..\. \ .\.  \ .\. "
  15 PRINT "   \ :\: \ :\: \::\ :\:. \::\ :\:'\':\: \::\''\':\ :\: \ :\: \ :\:. \.:\: "
  20 PRINT "   \ :\:.\::\' \::\ :\::\:.\::\ :\:   \:: \ :\ :\: \ :\: \ :\:'\::\':\: "
  25 PRINT "   \ :\:'\::\. \::\ :\: \':\::\ :\: \ .\. \:: \ :\ :\: \ :\: \ :\:  \ :\: "
  30 PRINT "   \ :\: \ :\: \::\ :\:  \::\ :\:.\..\: \::\..\.:\ :\:.\.:\: \ :\:  \ :\: "
  35 PRINT "   \ '\' \ '\' \''\ '\'  \''\ '\''\''\' \''\''\' \ '\''\''\' \ '\'  \ '\' "
  40 PRINT ,,
  50 PRINT "COPYRIGHT 1982-M0RGAN ASSOCIATES"
  60 PRINT AT 16,1;"QUIERES VER LAS INSTRUCCIONES?"
  65 PRINT AT 18,13;"S/N"
  70 IF INKEY$ ="" THEN GOTO 70
  80 IF INKEY$ ="S" THEN GOSUB 8990
 230 LET Y=0
 240 LET C=5000
 242 LET Q=INT (RND*3)+1
 244 IF Q=1 THEN LET C=C-250
 246 IF Q=2 THEN LET C=C-400
 248 IF Q=3 THEN LET C=C+150
 250 LET S=1000
 252 LET W=INT (RND*3)+1
 254 IF W=1 THEN LET S=S+50
 256 IF W=2 THEN LET S=S+80
 258 IF W=3 THEN LET S=S-75
 260 LET L=200
 300 LET Y=Y+1
 340 CLS
 345 LET K=0
 350 PRINT ,,"           %A%N%O   ";Y
 360 PRINT 
 370 PRINT "  TIENES ";C;" SACOS DE MAIZ"
 380 PRINT 
 400 PRINT "     TIENES ";S;" SUBDITOS"
 410 PRINT 
 420 PRINT "   TIENES ";L;" ACRES DE TIERRA"
 425 PRINT 
 430 PRINT "\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''"
 440 PRINT 
 445 IF Y=2 THEN GOTO 1810
 450 PRINT " CUANTOS SACOS DE MAIZ PLANTAS?"
 470 PRINT 
 480 INPUT P
 490 IF NOT P>C THEN GOTO 520
 500 PRINT "    NO TIENES TANTOS"
 505 LET K=K+1
 508 IF K=4 THEN GOTO 340
 510 GOTO 480
 520 IF NOT P>2*S THEN GOTO 550
 530 PRINT "    NO HAY TANTOS SUBDITOS"
 535 LET K=K+1
 538 IF K=4 THEN GOTO 340
 540 GOTO 480
 550 IF NOT P>(7+1)*L THEN GOTO 580
 560 PRINT "    NO HAY SUFICIENTE TIERRA"
 565 LET K=K+1
 568 IF K=4 THEN GOTO 340
 570 GOTO 480
 580 PRINT 
 585 LET C=C-P
 600 PRINT "   CUANTOS SACOS DE MAIZ"
 610 PRINT "   QUIERES DAR COMO COMIDA"
 620 PRINT "   TIENES ";C
 622 PRINT 
 630 INPUT F
 640 IF NOT F>C THEN GOTO 670
 650 PRINT "   NO TIENES TANTOS"
 655 LET K=K+1
 657 IF K=4 THEN LET C=C+P
 658 IF K=4 THEN GOTO 340
 660 GOTO 630
 670 LET C=C-F
 680 CLS
 700 LET Z=1
 710 LET A=P/(7+1)
 720 LET X=L*3/4
 730 IF A<X THEN LET Z=-1
 740 LET G=INT (Z*L/4)
 750 LET L=L+G
 760 LET ZP=1
 770 LET M=1
 780 LET E=F/4
 790 IF E<S THEN LET ZP=-1
 800 LET GP=(E-S)*ZP
 810 LET X=S*3/4
 820 IF E<X THEN LET M=-1
 830 LET S=E
 840 RAND 
 850 LET X=INT (RND*3)+1
 900 IF Z<0 THEN GOTO 940
 910 PRINT "TU REINO CRECE EN ";G;" ACRES"
 930 GOTO 950
 940 PRINT "    HAS PERDIDO ";G*Z;" ACRES"
 950 PRINT 
 951 IF L>12 THEN GOTO 960
 952 PRINT "\:'\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\':"
 953 PRINT "\:                               \ :"
 954 PRINT "\:      HAS PERDIDO LA PARTIDA   \ :"
 955 PRINT "\:      POR FALTA DE TIERRAS     \ :"
 956 PRINT "\:                               \ :"
 957 PRINT "\:.\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\.:"
 958 STOP
 960 IF GP=0 THEN GOTO 1290
 970 IF ZP<0 THEN GOTO 1000
 980 PRINT "    HAS GANADO ";GP;" SUBDITOS"
 990 GOTO 1010
1000 PRINT GP;" PERSONAS MUEREN DE HAMBRE"
1010 PRINT 
1020 IF S>24 THEN GOTO 1100
1030 PRINT "\:'\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\':"
1040 PRINT "\:                               \ :"
1050 PRINT "\:     HAS PERDIDO LA PARTIDA    \ :"
1060 PRINT "\:     POR FALTA DE SUBDITOS     \ :"
1070 PRINT "\:                               \ :"
1080 PRINT "\:.\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\..\.:"
1090 GOTO 2000
1100 IF M>0 THEN GOTO 1290
1110 PRINT "\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''"
1115 PRINT 
1120 PRINT "MUCHAS PERSONAS MUEREN DE HAMBRE"
1130 PRINT "SUFRES UN INTENTO DE ASESINATO"
1135 PRINT 
1140 PRINT "\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''"
1150 PRINT 
1160 PRINT "PULSA UNA TECLA PARA VER SI "
1170 PRINT "SOBREVIVES..."
1180 IF INKEY$ ="" THEN GOTO 1175
1200 IF X>1 THEN GOTO 1270
1210 PRINT ,,,"  \:'\''\''\''\''\''\''\''\''\''\':"
1220 PRINT ,"  \: ";"         \ :"
1230 PRINT ,"  \: ";" MUERES  ";"\ :"
1240 PRINT ,"  \: ";"         \ :"
1250 PRINT ,"  \:.\..\..\..\..\..\..\..\..\..\.:"
1252 FOR A=1 TO 30
1254 NEXT A
1260 GOTO 2500
1270 PRINT ,,,,"     **********************"
1280 PRINT "     *   OK SIGUES VIVO   *"
1285 PRINT "     **********************"
1290 PRINT 
1300 PRINT " PULSA UNA TECLA PARA CONTINUAR"
1310 IF INKEY$ ="" THEN GOTO 1310
1320 CLS
1400 IF P<1 THEN GOTO 300
1410 LET W=1
1420 LET B=1
1425 IF P<20 THEN GOTO 1580
1430 LET Q=INT (RND*6)+1
1440 LET H=(P/10)*2
1450 IF Q>2 THEN LET H=(P/10)*4
1460 IF Q=6 THEN LET H=(P/10)*(7+1)
1470 IF NOT H>(S/10)*16 THEN GOTO 1530
1480 LET B=-1
1490 LET N=H-(S/10)*16
1500 IF N>3200 THEN LET N=3200
1510 LET N=N*10
1520 LET H=(S/10)*16
1530 IF C/10+H>3200 THEN GOTO 1560
1540 LET C=C+H*10
1550 GOTO 1600
1560 LET W=-1
1570 GOTO 1600
1580 LET Q=3
1590 LET C=P*2+C
1620 PRINT 
1630 IF Q<3 THEN PRINT ,,,,,,"  LA COSECHA HA SIDO POBRE"
1640 IF Q>=3 AND Q<6 THEN PRINT ,,,,,,"  LA COSECHA HA SIDO MEDIA"
1650 IF Q=6 THEN PRINT ,,,,,,"   LA COSECHA HA SIDO BUENA"
1660 PRINT 
1670 PRINT "\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''\''"
1675 PRINT 
1680 IF B>0 THEN GOTO 1710
1690 PRINT "  ";N;" SACOS PERDIDOS POR FALTA"
1700 PRINT ,"DE MANO DE OBRA"
1705 PRINT 
1710 IF W>0 THEN GOTO 1770
1720 FOR F=1 TO 21
1724 PRINT AT 9,0;"                                "
1728 PRINT AT 9,0;"********************************"
1730 PRINT "*  TIENES MAS DE 32000 SACOS   *"
1740 PRINT "*      DE MAIZ HAS GANADO      *" 
1745 PRINT AT 12,0;"                                "
1750 PRINT AT 12,0;"********************************"
1755 NEXT F
1760 GOTO 2000
1770 PRINT ,,,,"PULSA UNA TECLA PARA CONTINUAR"
1780 IF INKEY$ ="" THEN GOTO 1780
1790 CLS
1800 GOTO 300
1810 FOR K=1 TO 21
1816 PRINT AT 11,0;"                                "
1818 PRINT AT 11,0;"********************************"
1820 PRINT "*   HAS SOBREVIVIDO 20 ANOS    *"
1830 PRINT "*          HAS GANADO          *"
1835 PRINT AT 14,0;"                                "
1840 PRINT AT 14,0;"********************************"
1850 NEXT K
2000 PRINT AT 19,0;"  PULSA \"R\" PARA JUGAR DE NUEVO"
2010 IF INKEY$ ="" THEN GOTO 2010
2020 IF INKEY$ ="R" THEN GOTO 230
2030 STOP
2500 CLS
2510 PRINT AT 5,4;"          \.:\:."
2520 PRINT "              \ :\: "
2530 PRINT "           \:'\''\''\''\''\''\':\.              "
2540 PRINT "           \:      \ :\'.            "
2550 LET L$="           \:      \ :\ :             "
2560 PRINT L$;L$;L$;L$;L$;L$;L$
2565 FOR L=1 TO 16
2570 PRINT "////////";
2575 NEXT L
2578 FOR M=1 TO 41
2580 PRINT AT 10,13;"   "
2584 PRINT AT 10,13;"RIP"
2586 NEXT M
2600 PRINT AT 20,0;"  PULSA \"R\" PARA JUGAR DE NUEVO"
2610 IF INKEY$ ="" THEN GOTO 2010
2620 IF INKEY$ ="R" THEN GOTO 230
2630 STOP
8990 CLS
8995 GOSUB 9150
9000 PRINT ,,"ERES EL GOBERNANTE DE UN PEQUENO";AT 14,0;"RENIO MEDIEVAL.";AT 16,0;"TIENES QUE DECIDIR CUANTO MAIZ";AT 17,0;"SEMBRAR Y CUANTO USAR";AT 18,0;"PARA ALIMENTAR A TUS SUBDITOS"
9020 PRINT AT 20,0;"PULSA UNA TECLA PARA CONTINUAR"
9030 IF INKEY$ ="" THEN GOTO 9030
9040 CLS
9050 PRINT "%M%.%I%. SE NECESITAN CUATRO SACOS"
9060 PRINT "DE MAIZ PARA ALIMENTAR UNA"
9070 PRINT "PERSONA POR UN ANO. PLANTAR UN"
9080 PRINT "ACRE CUESTA OCHO SACOS. UNA"
9090 PRINT "PERSONA PUEDE PLANTAR 2 SACOS."
9100 PRINT ,,,,"TIENES QUE SOBREVIVIR 20 ANOS O ACUMULAR 32.000 SACOS DE MAIZ   PARA GANAR"
9106 PRINT ,,,,"%C%U%I%D%A%D%O%: SI NO SIEMBRAS BASTANTEMAIZ, NO PUEDE ESPERAR UNA BUENACOSECHA"
9108 PRINT "PERO SI TUS SUBDITOS NO COMEN   SUFICIENTE... ATENTE A LAS CONSECUENCIAS, HAS SIDO ADVERTIDO"
9110 PRINT ,,,,,,"       OK PULSA UNA TECLA"
9120 IF INKEY$ ="" THEN GOTO 9120
9130 CLS
9132 PRINT AT 5,0;"%M%.%I%. RECUERDA:";
9133 PRINT AT 7,0;"CADA CAMPESINO CONSUME   4 SACOS";"PUEDE PLANTAR            2 SACOS";;"PLANTAR UN ACRE CUESTA   8 SACOS";
9136 PRINT ,,,,,,,,"PULSA UNA TECLA PARA JUGAR";
9138 IF INKEY$ ="" THEN GOTO 9138
9139 CLS
9140 RETURN
9150 DIM A$(6,32)
9160 LET A$(1)="\:: \:: \::                     \:: \:: \::"
9170 LET A$(2)="\ '\::\::\::\'                      \ '\::\::\::\' "
9180 LET A$(3)=" \:: \::                       \:: \:: "
9190 LET A$(4)=" \::\::\:: \:: \:: \:: \:: \:: \:: \:: \:: \:: \:: \:: \::\::\:: "
9200 LET A$(5)=" \::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::\::"
9210 LET A$(6)=" \::\::\::\::\::\::\::\::\::\::\::\::    \::\::\::\::\::\::\::\::\::\::\::\::\::"
9260 PRINT A$(1);A$(2);A$(3);A$(3);A$(4);A$(5);A$(5);A$(5);A$(6);A$(6);A$(6);A$(6)
9270 RETURN
