#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <zx81.h>
void in_Pause(uint msec);
char strlen(const char *s);
uint in_KeyPressed(uint scancode);
int in_Wait(uint msec);
unsigned int in_Inkey(void);
void in_WaitForKey(void);
void in_WaitForNoKey(void);

int in_InKey(const char *s);

extern int d_file @16396;

uchar GetChar(char cX, char cY)
{
	return wpeek(wpeek(16396) + 1 + cY + cX * 33);
}

void SetChar(char cX, char cY, char cValue) {
	bpoke(wpeek(16396) + 1 + cY + cX * 33, cValue);
}

void printBloq (char x, char y)
{
	SetChar(x, y,     129);
	SetChar(x, y + 1, 130);

	SetChar(x + 1, y,   132);
	SetChar(x + 1, y + 1, 7);
}

void printKey (char x, char y)
{
	SetChar(x, y,     0);
	SetChar(x, y + 1, 7);

	SetChar(x + 1, y,     135);
	SetChar(x + 1, y + 1, 130);
}

void deleteKey (char x, char y)
{
	SetChar(x, y + 1, 0);

	SetChar(x + 1, y,     0);
	SetChar(x + 1, y + 1, 0);
}

void printExit (char x)
{
	char y = 29;
	SetChar(x, 29, 136);
	SetChar(x, 30, 136);

	SetChar(x + 1, 29, 136);
	SetChar(x + 1, 30, 170);

	SetChar(x + 2, 29, 136);
	SetChar(x + 2, 30, 136);
}

void openExit (char x)
{
	SetChar(x, 29, 0);
	SetChar(x, 30, 0);

	SetChar(x + 1, 29, 0);

	SetChar(x + 2, 29, 0);
	SetChar(x + 2, 30, 0);
}

unsigned char xpos [];
unsigned char ypos [];
unsigned char a [];
unsigned char b [];
unsigned char c [];
unsigned char d [];

unsigned char PersonajeEneH = 1;
unsigned char PersonajeEneV = 2;
unsigned char PersonajeEneA = 3;

struct controls {
	unsigned char up;
	unsigned char down;
	unsigned char left;
	unsigned char right;
} k1;

unsigned char start;

#asm
	._xpos
		defb 0
	._ypos
		defb 0
	._a
		defb 0
	._b
		defb 0
	._c
		defb 0
	._d
		defb 0
#endasm

void printCharacterAsm(unsigned char x, unsigned char y, unsigned char pos)
{
	xpos [0] = x;
	ypos [0] = y;

	if (pos == 1) {
		c [0] = 129;
		d [0] = 134;
	} else {
		c [0] = 6;
		d [0] = 130;
	}

 	#asm 
		ld	hl, (16396)	; D_FILE

		ld a, (_xpos)
		and	a						; ¿a == 0?
		jr	z, z81tpzero

		ld b, a
		ld de, 33
	
	.z81tpl1
		add	hl, de
		djnz z81tpl1				; hl = buffer + 32 * x
	
	.z81tpzero
		ld a, (_ypos)
		add hl, a;	
		ld a, 155 					; punto invertido
		ld (hl),a

		add hl, 1
		ld b, 155 					; punto invertido
		ld (hl),b

		add hl, 32
		ld a, (_c)
		ld (hl),a

		add hl, 1	
		ld a, (_d)
		ld (hl),a

	#endasm;
}

void printEne(unsigned char x, unsigned char y, unsigned char type, unsigned char pos)
{
	xpos [0] = x;
	ypos [0] = y;

	if (type == 1) {
		//Enemigo vertical
		a [0] = 132;
		b [0] = 7;
		if (pos == 1) {
			c [0] = 129;
			d [0] = 7;
		} else {
			c [0] = 132;
			d [0] = 130;
		}
	} else if (type == 2) {
		// Enemigo horizontal
		a [0] = 130;
		b [0] = 129;
		if (pos == 1) {
			c [0] = 133; 
			d [0] = 134;
		} else {
			c [0] = 6;
			d [0] = 5;
		}
	} else {
		// Enemigo aleatorio
		a [0] = 132;
		b [0] = 7;
		if (pos == 1) {
			c [0] = 132; 
			d [0] = 128;
		} else {
			c [0] = 128;
			d [0] = 7;
		}
	}

 	#asm 
		ld	hl, (16396)	; D_FILE

		ld a, (_xpos)
		and	a						; ¿a == 0?
		jr	z, z81etpzero

		ld b, a
		ld de, 33
	
	.z81etpl1
		add	hl, de
		djnz z81etpl1				; hl = buffer + 32 * x
	
	.z81etpzero
		ld a, (_ypos)
		add hl, a;	
		ld a, (_a)
		ld (hl),a

		add hl, 1
		ld a, (_b)
		ld (hl),a

		add hl, 32
		ld a, (_c)
		ld (hl),a

		add hl, 1	
		ld a, (_d)
		ld (hl),a

	#endasm;
}

void deleteCharacterAsm(unsigned char x, unsigned char y) {
	xpos [0] = x;
	ypos [0] = y;
	
	#asm
	ld	hl, (16396)	; D_FILE

	ld a, (_xpos)
	and	a						; ¿a == 0 ?
	jr	z, z81tpzero1

	ld b, a
	ld de, 33

	.z81tpl2
	add	hl, de
	djnz z81tpl2				; hl = buffer + 32 * x

   .z81tpzero1
    ld a, (_ypos)
    add hl, a;

	ld b, 0
	ld (hl), b

	add hl, 1
	ld (hl), b

	add hl, 32
	ld (hl), b

	add hl, 1
	ld (hl), b

	#endasm

}

unsigned char menu_define_key() {
	//https://bitbucket.org/CmGonzalez/gandalf/src/0af70239a352257743d21292715470d116b2c4ab/game_menu.c#lines-199
	unsigned char tmp1;
    in_WaitForKey();
    tmp1 = in_Inkey();
    in_WaitForNoKey();
    return tmp1;
}

void printCenter(char numLiner, char* textCenter) {
	char lenCenter = strlen(textCenter);
	char whiteSpace = (32 - lenCenter) / 2;
	zx_setcursorpos(numLiner, whiteSpace);
	printf("%s", textCenter);
}

extern unsigned char xKey1 = 10, yKey1 = 5, xKey2 = 18, yKey2 = 10, xKey3 = 8, yKey3 = 15, exit = 5, key1 = 1, key2 = 1, key3 = 1, keys = 3;
extern unsigned char xEne1 = 1, yEne1 = 2, xEne2 = 18, yEne2 = 20, xEneA = 10, yEneA = 10, counter = 40, x = 1, y = 10;

void checkKey(char x, char y) {
	if ( key1 == 1 &&
	        ((x == xKey1 + 1 && (y - 1 == yKey1 + 1 || y - 1 == yKey1 || y == yKey1))
	      || (x == xKey1     && (y - 1 == yKey1 + 1 			      || y == yKey1))
	      || (x + 1 == xKey1 && (y - 1 == yKey1 + 1 || y - 1 == yKey1 || y == yKey1)))) {
		key1 = 0;
		deleteKey(xKey1, yKey1);
		keys--;
		if (keys == 0) {
			openExit(exit);
		}
	}

	if ( key2 == 1 &&
	        ((x == xKey2 + 1 && (y - 1 == yKey2 + 1 || y - 1 == yKey2 || y == yKey2))
          || (x == xKey2     && (y - 1 == yKey2 + 1 			      || y == yKey2))
          || (x + 1 == xKey2 && (y - 1 == yKey2 + 1 || y - 1 == yKey2 || y == yKey2)))) {
		key2 = 0;
		deleteKey(xKey2, yKey2);
		keys--;
		if (keys == 0) {
			openExit(exit);
		}
	}

	if ( key3 == 1 &&
	        ((x == xKey3 + 1 && (y - 1 == yKey3 + 1 || y - 1 == yKey3 || y == yKey3))
	      || (x == xKey3     && (y - 1 == yKey3 + 1 			      || y == yKey3))
	      || (x + 1 == xKey3 && (y - 1 == yKey3 + 1 || y - 1 == yKey3 || y == yKey3)))) {
		key3 = 0;	
		deleteKey(xKey3, yKey3);
		keys--;
		if (keys == 0) {
			openExit(exit);
		}
	}
}

char checkColision(char xEne, char yEne) {
	if (   (x == xEne + 1 && (y == yEne +1 || y == yEne || y + 1 == yEne)) 
		|| (x == xEne     && (y == yEne +1 || y == yEne || y + 1 == yEne))
		|| (x + 1 == xEne && (y == yEne +1 || y == yEne || y + 1 == yEne))) {
		return 1;
	}
	return 0;
}

void printMaze (char screen[9][15])
{	
	char a,b;

	zx_asciimode(0);
    for (a = 0; a < 9; ++a)
    {
        for (b = 0; b < 15; ++b)
        {
    		if (screen[a][b] == 1) {
				zx_setcursorpos((a*2)+1,(b*2)+1);
    			printf("%c%c", 136, 136);

				zx_setcursorpos((a*2)+2,(b*2)+1);
    			printf("%c%c", 136, 136);
    		}
			else if (screen[a][b] == 2) {
				zx_setcursorpos((a*2)+1,(b*2)+1);
    			printf("%c%c", 129, 130);

				zx_setcursorpos((a*2)+2,(b*2)+1);
    			printf("%c%c", 132, 7);
    		}
		}
	}
	zx_asciimode(1);
}

void printGood()
{
 	zx_setcursorpos(2, 10);
	printCenter(6, "      ");
	printCenter(7, " good ");
	in_Wait(100);
	printCenter(8, "          ");
	printCenter(9, " next stage ");
	printCenter(10, "            ");
	in_Wait(100);
	fgetc_cons();        // wait for keypress
}

void printEnd()
{
	zx_cls();
	printCenter(8, "congratulations");
	in_Wait(100);
	printCenter(10, "you are out of tokat dungeon");
	in_Wait(100);
	printCenter(15, "SEE YOU IN AMIDAR");
	in_Wait(200);
	fgetc_cons();        // wait for keypress
}

int main(void)
{
	k1.up = 'Q';
	k1.down = 'A';
	k1.left = 'O';
	k1.right = 'P';

	unsigned char pos = 1, stage = 1, lastStage = 1, lives = 3, cicle = 1, posEne = 1, posEneA = 1;
	char dEne1 = 1, dEne2 = -1, cEne1 = 1, cEne2 = 1, dEneA = 0, cEneA = 1;
	unsigned char cicleEne = 6, cicleDEne = 10, cicleCEne = 2, cicleEneA = 6, cicleDEneA = 10, cicleCEneA = 2;
RESTART:

	lives = 3;
	
	MENU:
	zx_cls();
	printCenter(3, "ESCAPE FROM TOKAT DUNGEON");
	zx_asciimode(0);
	zx_setcursorpos(2, 2);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128);
	zx_setcursorpos(3, 2);
	printf("%c", 128);
	zx_setcursorpos(3, 9);
	printf("%c", 128);
	zx_setcursorpos(3, 14);
	printf("%c", 128);
	zx_setcursorpos(3, 20);
	printf("%c", 128);
	zx_setcursorpos(3, 28);
	printf("%c", 128);
	zx_setcursorpos(4, 2);
	printf("%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128);
	zx_asciimode(1);
	printCenter(11, "press \"R\" to redefine keys");
	printCenter(13, "press \"I\" to instructions");
	printCenter(15, "press \"S\" to start");
	if (lastStage > 1 && lastStage < 25) {
		printCenter(17, "press \"C\" to start in last stage");
	}
	printCenter(23, "2022 salvacam");

SELECTOPTION:
	start = menu_define_key();

	if (start == 'C') {
		stage = lastStage;
		goto RESET;
	}

	if (start == 'S') {
		lastStage = stage;
		goto RESET;
	}

	if (start == 'I') {

		zx_cls();
		zx_setcursorpos(2, 5);
		printf("get all keys");
		zx_setcursorpos(5, 5);
		printf("go to exit");
		zx_setcursorpos(8, 5);
		printf("do not hit mines");
		zx_setcursorpos(11, 5);
		printf("or enemies");

		zx_asciimode(0);
		zx_setcursorpos(2, 18);
		printf("%c", 7);
		zx_setcursorpos(3, 17);
		printf("%c%c", 135, 130);
		
		zx_setcursorpos(5, 16);
		printf("%c", 170);

		zx_setcursorpos(7,22);
		printf("%c%c", 129, 130);
		zx_setcursorpos(8,22);
		printf("%c%c", 132, 7);


		zx_setcursorpos(11,16);
		printf("%c%c%c%c%c%c%c%c%c%c", 132, 7, 0, 0, 130, 129, 0, 0, 132, 7);
		zx_setcursorpos(12,16);
		printf("%c%c%c%c%c%c%c%c%c%c", 129, 7, 0, 0, 133, 134, 0, 0, 133, 128);

		zx_asciimode(1);

		printCenter(17, "controls");
		zx_setcursorpos(19, 5);
		printf("up:   %c", k1.up);
			
		zx_setcursorpos(21, 5);
		printf("down: %c", k1.down);

		zx_setcursorpos(19, 16);
		printf("left:  %c", k1.left);
		
		zx_setcursorpos(21, 16);
		printf("right: %c", k1.right);

		fgetc_cons();        // wait for keypress
		goto MENU;

	} else if (start == 'R') {
		REMAP:
		zx_cls();

		zx_setcursorpos(5, 5);
		printf("up:   ");
		k1.up = menu_define_key();
		printf("%c", k1.up);
			
		zx_setcursorpos(7, 5);
		printf("down: ");
		k1.down = menu_define_key();
		printf("%c", k1.down);

		zx_setcursorpos(5, 15);
		printf("left:  ");
		k1.left = menu_define_key();
		printf("%c", k1.left);
		
		zx_setcursorpos(7, 15);
		printf("right: ");
		k1.right = menu_define_key();
		printf("%c", k1.right);

		printCenter(12, "are you ok?");
		printCenter(14, "press \"Y\" to accept");
		printCenter(16, "press any to key to remap");
		start = menu_define_key();
		if (start != 'Y') goto REMAP;
		if (start == 'Y') goto MENU;
	}
	if (start != 'C' || start != 'S' ){
		goto SELECTOPTION;
	}


RESET:
	zx_cls();

	zx_asciimode(0);
	char i;
	for (i = 0; i < 32; ++i)
	{
		zx_setcursorpos(0, i);
		printf("%c", 136);

		zx_setcursorpos(21, i);
		printf("%c", 136);
	}	

	for (i = 0; i < 21; ++i)
	{
		zx_setcursorpos(i, 0);
		printf("%c", 136);

		zx_setcursorpos(i, 31);
		printf("%c", 136);
	}

	if (lives == 3) {
		zx_setcursorpos(22, 26);
		printf("%c%c", 155, 155);
		zx_setcursorpos(23, 26);
		printf("%c%c", 129, 130);
	} 

	if (lives >= 2) {
		zx_setcursorpos(22, 29);
		printf("%c%c", 155, 155);
		zx_setcursorpos(23, 29);
		printf("%c%c", 129, 130);
	}
	
	zx_asciimode(1);
	zx_setcursorpos(22, 4);
	printf("TIME");

	zx_setcursorpos(22, 14);
	printf("STAGE");
	zx_setcursorpos(23, 17);
	printf("%d", stage);

	char screen1[9][15] = {
		{0,0,0,0,0,0,0,0,0,1,0,1,0,0,0},
		{0,0,0,0,0,0,0,2,0,0,0,0,0,0,0},
		{0,0,2,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,2,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,1,0,1,0,2,0,0,0,0,0,0,0,0,0},
		{0,1,1,1,0,0,0,0,0,0,0,0,0,0,1},
		{0,0,0,0,0,0,0,0,0,0,2,0,0,0,0},
		{0,0,0,0,0,0,0,2,0,0,0,0,0,0,1}
	};
	
	char screen2[9][15] = {
		{0,0,0,0,0,0,0,0,0,0,0,1,0,0,0},
		{0,0,2,0,0,0,0,0,0,0,0,1,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,1,1,1,0,0,2,0,2,0,0,0,0,2,0},
		{0,0,0,0,0,0,0,2,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,1,1,1,1},
		{0,2,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{2,0,0,0,0,0,0,0,0,0,2,0,0,0,0},
		{0,2,0,0,0,0,0,0,0,2,0,0,0,0,0}
	};
	
	char screen3[9][15] = {
		{0,0,0,1,0,0,0,0,0,0,0,0,1,0,0},
		{0,0,0,1,0,0,0,2,0,0,1,0,1,0,0},
		{2,0,0,1,0,0,0,0,0,0,1,0,0,2,0},
		{0,0,0,0,0,0,0,0,0,0,1,0,0,0,0},
		{0,0,1,1,1,1,1,1,1,1,1,0,1,1,1},
		{0,0,2,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,2,0,0,0,0,2,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,1,1,1,1,1,1,1,1,1,1,0,0,0,0}
	};
	
	char screen4[9][15] = {
		{2,0,2,0,0,2,0,0,2,0,0,0,2,0,0},
		{0,0,0,0,2,0,0,0,0,2,0,0,1,0,2},
		{0,0,0,0,0,0,0,0,0,0,0,0,2,0,1},
		{0,0,0,0,2,0,0,0,0,0,0,0,1,0,0},
		{2,0,0,0,0,2,0,0,0,2,0,0,2,2,0},
		{0,0,0,0,0,0,2,1,2,0,0,0,2,1,0},
		{2,0,2,2,0,0,2,0,0,2,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,2,1,2,1,2,1,2,1,2,1,2,2,0,0}
	};

	char screen5[9][15] = {
		{2,2,0,0,0,0,0,2,0,2,0,0,0,0,0},
		{0,0,0,0,2,2,0,0,0,0,0,0,0,0,0},
		{2,2,0,0,0,0,0,1,0,1,0,0,0,0,0},
		{0,0,0,0,0,1,0,1,0,1,0,0,0,0,0},
		{1,1,1,0,0,1,0,1,0,1,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,1,0,0,0,0,0},
		{0,0,0,0,2,1,0,1,0,1,0,0,0,0,0},
		{0,0,0,0,0,1,0,1,0,1,0,1,1,1,1},
		{2,0,2,0,0,2,0,1,0,1,0,0,0,0,0}
	};

	char screen6[9][15] = {
		{0,0,0,0,0,0,0,0,0,0,0,0,1,0,0},
		{0,0,0,2,0,0,0,0,2,0,0,0,1,0,0},
		{0,1,1,1,1,1,1,1,1,1,1,1,1,0,0},
		{0,0,0,2,0,0,0,0,2,0,0,0,1,0,0},
		{0,0,0,2,0,2,2,2,2,0,0,0,1,0,0},
		{0,0,0,2,0,2,0,0,0,0,0,0,1,0,0},
		{1,1,0,0,0,0,0,0,0,1,0,0,1,0,0},
		{0,0,0,0,0,0,0,0,0,1,0,0,1,0,0},
		{0,0,0,0,0,0,0,0,0,1,0,0,1,0,0}
	};

	key1 = 1, key2 = 1, key3 = 1, keys = 3;

	switch(stage) {
		case 6: 
		case 12: 
		case 18: 
		case 24: 
		printMaze(screen6);
		xEne1 = 3, yEne1 = 28, xEne2 = 1, yEne2 = 20, xEneA = 16, yEneA = 15, 
		counter = 40, x = 19, y = 2;
		xKey1 = 7, yKey1 = 15, xKey2 = 1, yKey2 = 23, xKey3 = 1, yKey3 = 27, exit = 18;
		break;
		case 5: 
		case 11: 
		case 17: 
		case 23: 
		printMaze(screen5);
		xEne1 = 1, yEne1 = 22, xEne2 = 11, yEne2 = 10, xEneA = 1, yEneA = 10, 
		counter = 35, x = 19, y = 30;
		xKey1 = 3, yKey1 = 1, xKey2 = 17, yKey2 = 3, xKey3 = 1, yKey3 = 17, exit = 3;
		break;
		case 4: 
		case 10: 
		case 16: 
		case 22: 
		printMaze(screen4);
		xEne1 = 4, yEne1 = 4, xEne2 = 15, yEne2 = 24, xEneA = 6, yEneA = 14,
		counter = 30, x = 1, y = 30;
		xKey1 = 11, yKey1 = 1, xKey2 = 19, yKey2 = 23, xKey3 = 1, yKey3 = 3, exit = 17;
		break;
		case 3: 
		case 9: 
		case 15: 
		case 21: 
		printMaze(screen3);
		xEne1 = 18, yEne1 = 24, xEne2 = 7, yEne2 = 2, xEneA = 12, yEneA = 10,
		counter = 25, x = 19, y = 2;
		xKey1 = 3, yKey1 = 3, xKey2 = 17, yKey2 = 28, xKey3 = 11, yKey3 = 7, exit = 1;
		break;
		case 2: 
		case 8: 
		case 14: 
		case 20: 
		printMaze(screen2);
		xEne1 = 11, yEne1 = 10, xEne2 = 11, yEne2 = 2, xEneA = 14, yEneA = 14, 
		counter = 25, x = 1, y = 2;
		xKey1 = 15, yKey1 = 3, xKey2 = 3, yKey2 = 27, xKey3 = 7, yKey3 = 15, exit = 16;
		break;
		default: printMaze(screen1);
		xEne1 = 5, yEne1 = 26, xEne2 = 9, yEne2 = 2, xEneA = 12, yEneA = 10, 
		counter = 20, x = 1, y = 2;
		xKey1 = 11, yKey1 = 5, xKey2 = 1, yKey2 = 21, xKey3 = 15, yKey3 = 29, exit = 3;
	}


	switch(stage) {
		case 17: 
		case 18: 
		case 23: 
		case 24: 		
		cicleEne = 2;
		cicleDEne = 8;
		cicleCEne = 4;
		cicleEneA = 3, cicleDEneA = 8, cicleCEneA = 4;
		case 15: 
		case 16: 
		case 21: 
		case 22: 
		cicleEne = 3;
		cicleDEne = 9;
		cicleCEne = 3;
		cicleEneA = 5, cicleDEneA = 9, cicleCEneA = 3;
		case 13: 
		case 14: 
		case 19: 
		case 20: 
		cicleEne = 4;
		cicleDEne = 10;
		cicleCEne = 2;
		cicleEneA = 6, cicleDEneA = 10, cicleCEneA = 2;
		break;
		case 5: 
		case 6: 
		case 11: 
		case 12: 		
		cicleEne = 4;
		cicleEneA = 4, cicleDEneA = 8, cicleCEneA = 4;
		break;
		case 3: 
		case 4: 
		case 9: 
		case 10: 
		cicleEne = 5;
		cicleEneA = 5, cicleDEneA = 9, cicleCEneA = 3;
		break;
		case 1: 
		case 2: 
		case 7: 
		case 8: 
		cicleEne = 6;
		cicleEneA = 6, cicleDEneA = 10, cicleCEneA = 2;
		break;
		default: 
		cicleEne = 6;
		cicleDEne = 10;
		cicleCEne = 2;
		cicleEneA = 6, cicleDEneA = 10, cicleCEneA = 2;
	}

	cicle = 1;

	zx_setcursorpos(23, 5);
	printf("%d ", counter);

	printCharacterAsm(x, y, pos);

	printKey(xKey1, yKey1);
	printKey(xKey2, yKey2);
	printKey(xKey3, yKey3);

	printExit(exit);

INITGAME:
	
	cicle++;
	if (cicle >= 250) {
		cicle = 1;
	}

	in_Wait(10);

	if (in_KeyPressed(in_LookupKey(k1.left)) && GetChar(x, y-2) != 136 && GetChar(x+1, y-2) != 136) {  
		pos = 1 - pos;
		deleteCharacterAsm(x, y);
		y--;
		checkKey(x,y);
		if (GetChar(x, y-1) != 0 || GetChar(x+1, y-1) != 0) {
			lives--;
			if (lives == 0) {
				stage = 1;
				goto RESTART;
			}
			goto RESET;
		}
		printCharacterAsm(x, y, pos);
	}

	if (in_KeyPressed(in_LookupKey(k1.right)) && GetChar(x, y+1) != 136 && GetChar(x+1, y+1) != 136) { 
		pos = 1 - pos;
		deleteCharacterAsm(x, y);
		y++;
		checkKey(x,y);

		if (GetChar(x, y) == 170 || GetChar(x+1, y) == 170 || GetChar(x+1, y) == 170) {
			stage++;
			lastStage = stage;
			if (stage > 24) {
				zx_setcursorpos(2, 10);
				printEnd();
				stage = 1, lives = 3;
				goto RESTART;
			}
			printGood();
			goto RESET;
		}
		
		if (GetChar(x, y) != 0 || GetChar(x+1, y) != 0) {
			lives--;
			if (lives == 0) {				
				stage = 1;
				goto RESTART;
			}
			goto RESET;
		}
		printCharacterAsm(x, y, pos);
	}

	if (in_KeyPressed(in_LookupKey(k1.up)) && GetChar(x-1, y) != 136 && GetChar(x-1, y-1) != 136) { 
		pos = 1 - pos;
		deleteCharacterAsm(x, y);
		x--;
		checkKey(x,y);

		if (GetChar(x, y) == 170) {
			stage++;
			lastStage = stage;
			if (stage > 24) {
				zx_setcursorpos(2, 10);
				printEnd();
				stage = 1, lives = 3;
				goto RESTART;
			}
			printGood();
			goto RESET;
		}

		if (GetChar(x, y) != 0 || GetChar(x, y-1) != 0) {
			lives--;
			if (lives == 0) {
				stage = 1;
				goto RESTART;
			}
			goto RESET;
		}
		printCharacterAsm(x, y, pos);
	}

	if (in_KeyPressed(in_LookupKey(k1.down)) && GetChar(x+2, y) != 136 && GetChar(x+2, y-1) != 136) { 
		pos = 1 - pos;
		deleteCharacterAsm(x, y);
		x++;
		checkKey(x,y);

		if (GetChar(x+1, y) == 170) {
			stage++;
			lastStage = stage;
			if (stage > 24) {
				zx_setcursorpos(2, 10);
				printEnd();
				stage = 1, lives = 3;
				goto RESTART;
			}
			printGood();
			goto RESET;
		}

		if (GetChar(x+1, y) != 0 || GetChar(x+1, y-1) != 0) {
			lives--;
			if (lives == 0) {
				stage = 1;
				goto RESTART;
			}
			goto RESET;
		}
		printCharacterAsm(x, y, pos);
	}

	if (cicle % 15 == 0 ) {
		counter--;
		zx_setcursorpos(23, 5);
		printf("%d ", counter);
		if (counter == 0) {
			lives--;
			if (lives == 0) {
				stage = 1;
				goto RESTART;
			}
			goto RESET;
		}
	}

	if (stage <= 12) {
		if (cicle % cicleEne == 0 ) { 
			posEne = 1 - posEne;

			deleteCharacterAsm(xEne1, yEne1);
			xEne1 += dEne1;
			if (checkColision(xEne1, yEne1) == 1) {
				lives--;
				if (lives == 0) {
					stage = 1;
					goto RESTART;
				}
				goto RESET;
			}
			if (GetChar(xEne1 + 1, yEne1) != 0 || GetChar(xEne1 + 1, yEne1 - 1) != 0) {
				xEne1 -= dEne1;
				dEne1 = -1;
			} else if (GetChar(xEne1, yEne1) != 0 || GetChar(xEne1, yEne1 - 1) != 0) { 
				xEne1 -= dEne1;
				dEne1 = 1;
			}
			printEne(xEne1, yEne1, PersonajeEneH, posEne);

			deleteCharacterAsm(xEne2, yEne2);
			yEne2 += dEne2;
			if (checkColision(xEne2, yEne2) == 1) {
				lives--;
				if (lives == 0) {
					stage = 1;
					goto RESTART;
				}
				goto RESET;
			}
			if (GetChar(xEne2, yEne2 - 1) != 0 || GetChar(xEne2 + 1, yEne2 - 1) != 0) {
				yEne2 -= dEne2;
				dEne2 = 1;
			} else if (GetChar(xEne2, yEne2) != 0 || GetChar(xEne2 + 1, yEne2) != 0) { 
				yEne2 -= dEne2;
				dEne2 = -1;
			}
			printEne(xEne2, yEne2, PersonajeEneV, posEne);
		} 
	} else {
		if (cicle % cicleEne == 0 ) { 
			posEne = 1 - posEne;

			if (cicle % cicleDEne == 0 ) { 
				if (rand() % 2 == 0) {
					dEne1 = -dEne1;
				} 
			}

			cEne1 = (rand() % cicleCEne) + 1; 

			while (cEne1 > 0) {
				cEne1--;
				deleteCharacterAsm(xEne1, yEne1);
				xEne1 += dEne1;
				if (checkColision(xEne1, yEne1) == 1) {
					lives--;
					if (lives == 0) {
						stage = 1;
						goto RESTART;
					}
					goto RESET;
				}
				if (GetChar(xEne1 + 1, yEne1) != 0 || GetChar(xEne1 + 1, yEne1 - 1) != 0) {
					xEne1 -= dEne1;
					dEne1 = -1;
				} else if (GetChar(xEne1, yEne1) != 0 || GetChar(xEne1, yEne1 - 1) != 0) { 
					xEne1 -= dEne1;
					dEne1 = 1;
				}
				printEne(xEne1, yEne1, PersonajeEneH, posEne);
			}

			//randon sentido 
			if (cicle % cicleDEne == 0 ) { 
				if (rand() % 2 == 0) {
					dEne2 = -dEne2;
				} 
			}

			cEne2 = (rand() % cicleCEne) +1; 

			while (cEne2 > 0) {
				cEne2--;
				deleteCharacterAsm(xEne2, yEne2);
				yEne2 += dEne2;
				if (checkColision(xEne2, yEne2) == 1) {
					lives--;
					if (lives == 0) {
						stage = 1;
						goto RESTART;
					}
					goto RESET;
				}
				if (GetChar(xEne2, yEne2 - 1) != 0 || GetChar(xEne2 + 1, yEne2 - 1) != 0) {
					yEne2 -= dEne2;
					dEne2 = 1;
				} else if (GetChar(xEne2, yEne2) != 0 || GetChar(xEne2 + 1, yEne2) != 0) {
					yEne2 -= dEne2;
					dEne2 = -1;
				}
				printEne(xEne2, yEne2, PersonajeEneV, posEne);
			}
		}
	}

	if ((stage >= 7 && stage <= 12) || (stage >= 19 && stage <= 24)) { 
		//Enemigo aleatorio
		if (cicle % cicleEneA == 0 ) { 
			posEneA = 1 - posEneA;

			//randon sentido
			if (cicle % cicleDEneA == 0 ) { 
				dEneA = rand() % 4;
			}

			cEneA = (rand() % cicleCEneA) +1; 

			while (cEneA > 0) {				
				cEneA--;

				deleteCharacterAsm(xEneA, yEneA);

				if (dEneA == 2 || dEneA == 0) {
					if (dEneA == 2) xEneA += 1;
					if (dEneA == 0) xEneA -= 1;
					if (checkColision(xEneA, yEneA) == 1) {
						lives--;
						if (lives == 0) {
							stage = 1;
							goto RESTART;
						}
						goto RESET;
					}
					if (GetChar(xEneA + 1, yEneA) != 0 || GetChar(xEneA + 1, yEneA - 1) != 0) {
						xEneA -= 1;
						dEneA = 0;
					} else if (GetChar(xEneA, yEneA) != 0 || GetChar(xEneA, yEneA - 1) != 0) { 
						xEneA += 1;
						dEneA = 2;
					}
				}

				if (dEneA == 1 || dEneA == 3) {					
					if (dEneA == 1) yEneA += 1;
					if (dEneA == 3) yEneA -= 1;
					if (checkColision(xEneA, yEneA) == 1) {
						lives--;
						if (lives == 0) {
							stage = 1;
							goto RESTART;
						}
						goto RESET;
					}
					if (GetChar(xEneA, yEneA - 1) != 0 || GetChar(xEneA + 1, yEneA - 1) != 0) { 
						yEneA += 1;
						dEneA = 3;
					} else if (GetChar(xEneA, yEneA) != 0 || GetChar(xEneA + 1, yEneA) != 0) {
						yEneA -= 1;
						dEneA = 1;
					}
				}
				
				printEne(xEneA, yEneA, PersonajeEneA, posEneA);
			}
		}
	}

	goto INITGAME;	

	return 0;
}
