#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <zx81.h>
void in_Pause(uint msec);
char strlen(const char *s);
uint in_KeyPressed(uint scancode);
int in_Wait(uint msec);
unsigned int in_Inkey(void);
void in_WaitForKey(void);
void in_WaitForNoKey(void);

int in_InKey(const char *s);

extern int d_file @16396;

unsigned char xpos [];
unsigned char ypos [];
unsigned char a [];
unsigned char b [];
unsigned char c [];
unsigned char d [];

#asm
	._xpos
		defb 0
	._ypos
		defb 0
	._a
		defb 0
	._b
		defb 0
	._c
		defb 0
	._d
		defb 0
#endasm

void printCenter(char numLiner, char* textCenter) {
	char lenCenter = strlen(textCenter);
	char whiteSpace = (32 - lenCenter) / 2;
	zx_setcursorpos(numLiner, whiteSpace);
	printf("%s", textCenter);
}

void printTennis(char p)
{
	xpos [0] = p;
	
	#asm
	ld	hl, (16396)	; D_FILE

	ld a, (_xpos)
	and	a						; ¿a == 0 ?
	jr	z, z81tpzero

	ld b, a
	ld de, 33

	.z81tpl1
	add	hl, de
	djnz z81tpl1				; hl = buffer + 32 * x

   .z81tpzero
    ld a, 1
    add hl, a;

	ld b, 128
	ld (hl), b

	add hl, 33
	ld (hl), b

	add hl, 33
	ld (hl), b

	#endasm
}

void deleteTennis(unsigned char p)
{
	xpos [0] = p;
	
	#asm
	ld	hl, (16396)	; D_FILE

	ld a, (_xpos)
	and	a						; ¿a == 0 ?
	jr	z, z81tpzero1

	ld b, a
	ld de, 33

	.z81tpl2
	add	hl, de
	djnz z81tpl2				; hl = buffer + 32 * x

   .z81tpzero1
    ld a, 1
    add hl, a;

	ld b, 0
	ld (hl), b

	add hl, 33
	ld (hl), b

	add hl, 33
	ld (hl), b

	#endasm
}

void printTennisEne(char p)
{
	xpos [0] = p;
	
	#asm
	ld	hl, (16396)	; D_FILE

	ld a, (_xpos)
	and	a						; ¿a == 0 ?
	jr	z, z81tpzero0

	ld b, a
	ld de, 33

	.z81tpl10
	add	hl, de
	djnz z81tpl10				; hl = buffer + 32 * x

   .z81tpzero0
    ld a, 32
    add hl, a;

	ld b, 128
	ld (hl), b

	add hl, 33
	ld (hl), b

	add hl, 33
	ld (hl), b

	#endasm
}

void deleteTennisEne(char p)
{
	xpos [0] = p;
	
	#asm
	ld	hl, (16396)	; D_FILE

	ld a, (_xpos)
	and	a						; ¿a == 0 ?
	jr	z, z81tpzero3

	ld b, a
	ld de, 33

	.z81tpl13
	add	hl, de
	djnz z81tpl13				; hl = buffer + 32 * x

   .z81tpzero3
    ld a, 32
    add hl, a;

	ld b, 0
	ld (hl), b

	add hl, 33
	ld (hl), b

	add hl, 33
	ld (hl), b

	#endasm
}

void printBall(unsigned char x, unsigned char y)
{
	xpos [0] = x;
	ypos [0] = y+1;
	
	#asm
	ld	hl, (16396)	; D_FILE

	ld a, (_xpos)
	and	a						; ¿a == 0 ?
	jr	z, z81tpzer

	ld b, a
	ld de, 33

	.z81tpl
	add	hl, de
	djnz z81tpl				; hl = buffer + 32 * x

   .z81tpzer
    ld a, (_ypos)
    add hl, a;

	ld b, 180
	ld (hl), b

	#endasm
}

void deleteBall(unsigned char x, unsigned char y)
{	
	xpos [0] = x;
	ypos [0] = y+1;
	
	#asm
	ld	hl, (16396)	; D_FILE

	ld a, (_xpos)
	and	a						; ¿a == 0 ?
	jr	z, z81tpzero4

	ld b, a
	ld de, 33

	.z81tpl14
	add	hl, de
	djnz z81tpl14				; hl = buffer + 32 * x

   .z81tpzero4
    ld a, (_ypos)
    add hl, a;

	ld b, 0
	ld (hl), b

	#endasm
}


int main(void)
{	
	unsigned int hiscore = 0;
	char p = 10, e = 10, timeSleep = 10, cicle = 0, xd = 1, yd = 1;
	unsigned char x = 15, y = 10;

	RESTART:

	zx_cls();
	
	zx_asciimode(1);
	printCenter(4, "I PONG");
	zx_asciimode(0);
	zx_setcursorpos(3, 12);
	printf("%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128);
	zx_setcursorpos(4, 14);
	printf("%c", 128);
	zx_setcursorpos(4, 12);
	printf("%c", 128);
	zx_setcursorpos(4, 19);
	printf("%c", 128);
	zx_setcursorpos(5, 12);
	printf("%c%c%c%c%c%c%c%c", 128,128,128,128,128,128,128,128);
	zx_asciimode(1);

	zx_setcursorpos(10, 13);
	printf("Q up");
	zx_setcursorpos(12, 13);
	printf("A down");
	zx_setcursorpos(14, 13);
	printf("H end");
	
	printCenter(18, "press any key to start");
	
	if (hiscore > 0) {
		printCenter(20, "hi-score:  -   ");
		zx_setcursorpos(20, 20);
		printf("%d", hiscore);
	}

	printCenter(23, "2023 salvacam");

	fgetc_cons();        // wait for keypress
	zx_cls();

	zx_asciimode(0);

	
RESET:
	char g = 0, i;
	for (i = 0; i < 32; ++i)
	{
		zx_setcursorpos(0, i);
		printf("%c", 136);

		zx_setcursorpos(23, i);
		printf("%c", 136);
	}
	
	zx_asciimode(1);
	zx_setcursorpos(0, 3);
	printf("0");
	zx_setcursorpos(0, 28);
	printf("0");	
	zx_asciimode(0);

INITGAME:
	p = 10;
	printTennis(p);

	e = 10;
	printTennisEne(e);

	timeSleep = 10;	

	x = 15, y = 10, cicle = 0;
	xd = 1, yd = 1;

	if (rand() % 2 == 0) {
		xd = -xd;
	}

	if (rand() % 2 == 0) {
		yd = -yd;		
	}

BUCLE:
	
	cicle++;
	if (cicle >= 50) {
		cicle = 0;
		if (timeSleep > 1) timeSleep--;
	}

	in_Wait(timeSleep);

	if (in_KeyPressed(in_LookupKey('Q')) && p > 1) {  
		deleteTennis(p);
		p--;
		printTennis(p);
	}

	if (in_KeyPressed(in_LookupKey('A')) && p < 20) {  
		deleteTennis(p);
		p++;
		printTennis(p);
	}

	if (in_KeyPressed(in_LookupKey('H'))) {  
		if (g > hiscore) {
			hiscore = g;
		}
		goto RESTART;
	}

	deleteBall(x,y);

	if (x + xd < 1 || x + xd > 22) xd=-xd;
	x+=xd;

	if (y + yd < 1 && x != p && x != p+1 && x != p+2) {
		goto GOL;
	}	
	if (y + yd < 1 || y + yd >= 31) yd=-yd;
	y+=yd;

	//zx_setcursorpos(0, 1);
	//printf("%c", 0);

	if (yd > 0) {
		//zx_setcursorpos(0, 1);
		//printf("%c", 129);
		if (x > e && e < 20) {
			deleteTennisEne(e);
			e++;
			printTennisEne(e);
		}
		else if (x < e && e > 1) {
			deleteTennisEne(e);
			e--;
			printTennisEne(e);
		}
	}

	printBall(x,y);

	goto BUCLE;

	GOL:

	zx_asciimode(1);
	zx_setcursorpos(0, 28);
	printf("%d", ++g);
	zx_asciimode(1);

	deleteTennis(p);
	deleteTennisEne(e);

	goto INITGAME;

	zx_asciimode(1);

	return 0;
}
