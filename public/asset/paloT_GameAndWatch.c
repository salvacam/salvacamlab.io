#include <stdio.h>
#include <stdlib.h>
#include <zx81.h>
void in_Wait(uint msec);
char strlen(const char *s);
uint in_KeyPressed(uint scancode);

uchar GetChar(char cY, char cX)
{
	return wpeek(wpeek(16396) + 1 + cX + cY * 33);
}

void SetChar(char cX, char cY, char cValue) {
	bpoke(wpeek(16396) + 1 + cY + cX * 33, cValue);
}

void printScene()
{
	char i;
	for (i = 0; i < 29; ++i)
	{
		SetChar(19, i, 10);
	}

	SetChar(18, 0, 5);
	SetChar(17, 0, 5);
	SetChar(16, 0, 7);

	SetChar(18, 1, 133);
	SetChar(17, 1, 129);
	SetChar(16, 1, 132);

	SetChar(15, 0, 5);
	for (i = 14; i > 9; i = i - 4)
	{
		SetChar(i, 0, 130);
		SetChar(i, 1, 129);
		SetChar(i - 1, 0, 130);
		SetChar(i - 1, 1, 131);
		SetChar(i - 2, 0, 5);
		SetChar(i - 3, 0, 5);
	}

	SetChar(6, 0, 7); //128
	SetChar(6, 1, 3); //128


	zx_setcursorpos(20, 0);
	zx_asciimode(1);
	printf("<left");

	zx_setcursorpos(20, 23);
	printf("right>");
	zx_setcursorpos(21, 26);
	zx_asciimode(0);
	printf("%c", 164);
	zx_setcursorpos(21, 1);
	printf("%c", 161);

	SetChar(14, 26, 6);
	SetChar(15, 25, 6);
	SetChar(14, 27, 134);
	SetChar(15, 28, 134);

	SetChar(18, 27, 0);
	SetChar(17, 27, 0);
	SetChar(16, 27, 0);

	SetChar(18, 25, 5);
	SetChar(17, 25, 130);
	SetChar(16, 25, 7);

	SetChar(18, 26, 133);
	SetChar(17, 26, 133);
	SetChar(16, 26, 132);

	SetChar(16, 28, 133);
	SetChar(17, 28, 133);
	SetChar(18, 28, 133);

	zx_asciimode(1);

	zx_setcursorpos(1, 1);
	printf("PRESS ANY KEY TO START");

	zx_setcursorpos(3, 1);
	zx_asciimode(1);
	printf("hi-score");
	zx_asciimode(0);
}

void printHiscore(int score)
{
	zx_asciimode(1);
	if (score < 10) {
		zx_setcursorpos(3, 10);
		printf("000");
		zx_setcursorpos(3, 13);
		printf("%d", score);
	}
	else if (score < 100) {
		zx_setcursorpos(3, 10);
		printf("00");
		zx_setcursorpos(3, 12);
		printf("%d", score);
	}
	else if (score < 1000) {
		zx_setcursorpos(3, 10);
		printf("0");
		zx_setcursorpos(3, 11);
		printf("%d", score);
	}
	else {
		zx_setcursorpos(3, 10);
		printf("%d", score);
	}
	zx_asciimode(0);
}

void printScore(int score)
{
	zx_asciimode(1);
	if (score < 10) {
		zx_setcursorpos(3, 23);
		printf("%d", score);
	}
	else if (score < 100) {
		zx_setcursorpos(3, 22);
		printf("%d", score);
	}
	else if (score < 1000) {
		zx_setcursorpos(3, 21);
		printf("%d", score);
	}
	else {
		zx_setcursorpos(3, 20);
		printf("%d", score);
	}
	zx_asciimode(0);
}

void deleteAllShield(char h, char x)
{
	SetChar(h, (x * 4) + 1, 0);
	SetChar(h, (x * 4) + 2, 0);
	SetChar(h, (x * 4) + 3, 0);

	SetChar(h + 1, (x * 4) + 1, 0);
	SetChar(h + 1, (x * 4) + 2, 0);
	SetChar(h + 1, (x * 4) + 3, 0);

	SetChar(h + 2, (x * 4) + 1, 0);
	SetChar(h + 2, (x * 4) + 2, 0);
	SetChar(h + 2, (x * 4) + 3, 0);
}

void deleteShield(char h, char x)
{
	SetChar(h, (x * 4) + 1, 0);
	SetChar(h, (x * 4) + 2, 0);
	SetChar(h, (x * 4) + 3, 0);

	SetChar(h + 1, (x * 4) + 1, 0);
	SetChar(h + 1, (x * 4) + 2, 0);
	SetChar(h + 1, (x * 4) + 3, 0);
}

void printShield (char h, char x)
{
	SetChar(h, (x * 4) + 1, 130);
	SetChar(h, (x * 4) + 2, 128);
	SetChar(h, (x * 4) + 3, 129);


	SetChar(h + 1, (x * 4) + 1, 132);
	SetChar(h + 1, (x * 4) + 2, 49); // 177
	SetChar(h + 1, (x * 4) + 3, 7);


	SetChar(h + 2, (x * 4) + 1, 2);
	SetChar(h + 2, (x * 4) + 2, 128);
	SetChar(h + 2, (x * 4) + 3, 1);
}

void printCharacter (char y)
{
	SetChar(17, (y * 4) + 2,  139);
	char j = 133;

	switch (y) {
	case 1:
		j = 129;
		break;

	case 2:
	case 5:
		j = 130;
		break;

	case 3:
		j = 6;
		break;

	case 4:
		j = 134;
		break;

	case 6:
		j = 5;
		break;
	}

	SetChar(18, (y * 4) + 2, j);
}

void deleteCharacter (char y)
{
	SetChar(17, (y * 4) + 2,   0);
	SetChar(18, (y * 4) + 2,   0);
}

void printDead()
{
	SetChar(20, 14, 139);
	SetChar(20, 15, 131);
}

void deleteDead()
{
	SetChar(20, 14, 0);
	SetChar(20, 15, 0);
}

void openDoor()
{
	SetChar(18, 26, 0);
	SetChar(17, 26, 0);

	SetChar(18, 25, 5);
	SetChar(17, 25, 5);
	SetChar(16, 25, 7);
	SetChar(16, 26, 3);

	SetChar(18, 27, 5);
	SetChar(17, 27, 5);
	SetChar(16, 27, 5); //7
}

void closeDoor()
{
	SetChar(18, 27, 0);
	SetChar(17, 27, 0);
	SetChar(16, 27, 0);

	SetChar(18, 25, 5);
	SetChar(17, 25, 130);
	SetChar(16, 25, 7);

	SetChar(18, 26, 133);
	SetChar(17, 26, 133);
	SetChar(16, 26, 132);

	SetChar(16, 28, 133);
	SetChar(17, 28, 133);
	SetChar(18, 28, 133);
}

void live(char miss)
{
	if (miss == 1) {
		zx_setcursorpos(3, 26);
		zx_asciimode(1);
		printf("miss");
		zx_asciimode(0);
		SetChar(1, 29, 139);
	}
	else if (miss == 2) {
		SetChar(1, 27, 139);
	}
	else if (miss == 3) {
		SetChar(1, 25, 139);
	}
}

void printStart()
{
	zx_asciimode(1);

	zx_setcursorpos(1, 1);
	printf("                      ");

	zx_setcursorpos(0, 8);
	printf("         ");

	zx_asciimode(0);

	deleteDead();

	zx_setcursorpos(3, 26);
	zx_asciimode(1);
	printf("    ");
	zx_asciimode(0);
	SetChar(1, 29, 0);
	SetChar(1, 27, 0);
	SetChar(1, 25, 0);

	zx_asciimode(1);
	zx_setcursorpos(3, 20);
	printf("0000");
	zx_asciimode(0);
}

void printGameOver() {
	printDead();
	zx_setcursorpos(0, 8);
	zx_asciimode(1);
	printf("game over");
	zx_asciimode(0);
}

void printLiveLess() {
	printDead();
	in_Wait(150);
	deleteDead();
}

int main(void)
{

	int hiscore = 0;

RESET:

	char y = 0, cicleMove = 0, x = 0, moveV = 0, open = 0, h = 6, i = 0, miss = 0, go = 0, num = 9, initDoor = 1;
	unsigned char vel = 250, cicle = 250, cicleDoor = 0;
	int cicleGo = 0, score = 0;

	for (i = 1; i < 6; ++i)
	{
		deleteAllShield(6, i);
		deleteAllShield(8, i);
		deleteAllShield(10, i);
		deleteAllShield(12, i);
		deleteAllShield(14, i);
	}

	printScene();

	printHiscore(hiscore);

	fgetc_cons();        // wait for keypress

	printStart();

	printScore(score);

	printCharacter(y);

INITGAME:

	cicle++;
	cicleMove++;
	cicleDoor++;
	if (y == 0)
	{
		cicleGo++;

		if (cicleGo >= 1000) {
			go = 1;
			cicleGo = 0;
		}
	}

	if (cicleMove % 35 == 0 ) {
		moveV = 0;
		cicleMove = 0;
	}

	if (in_KeyPressed(in_LookupKey('5')) && y > 1 && moveV == 0) {
		deleteCharacter(y);
		y--;
		printCharacter(y);
		if (GetChar(14, (y * 4) + 1) == 130) {
			++miss;
			if (miss > 3)
			{
				deleteCharacter(y);
				printGameOver();
				if (score > hiscore) {
					hiscore = score;
					printHiscore(hiscore);
				}
				in_Wait(250);
				goto RESET;
			}
			live(miss);
			deleteCharacter(y);
			printLiveLess();
			y = 0;
			go = 0;
			printCharacter(y);
		}
		moveV = 1;
	}

	if ((in_KeyPressed(in_LookupKey('8')) && (y < 5 || (y < 6 && open == 1)) && moveV == 0) || (y == 0 && go == 1)) {
		deleteCharacter(y);
		y++;
		printCharacter(y);
		if (y == 6)
		{
			score = score + 5;
			if (score >= 10000) score = 0;
			printScore(score);
			deleteCharacter(y);
			y = 0;
			printCharacter(y);
			go = 0;
		}
		else if (GetChar(14, (y * 4) + 1) == 130) {
			++miss;
			if (miss > 3)
			{
				deleteCharacter(y);
				printGameOver();
				if (score > hiscore) {
					hiscore = score;
					printHiscore(hiscore);
				}
				in_Wait(250);
				goto RESET;
			}
			live(miss);
			deleteCharacter(y);
			printLiveLess();
			y = 0;
			go = 0;
			printCharacter(y);
		}
		moveV = 1;
	}

	if (cicleDoor >= 250)
	{
		cicleDoor = 0;

		if (initDoor == 1) {
			initDoor = 0;
			open = 1;
			openDoor();
		}
		else {
			x = rand() % 3;
			if ( x == 2 ) {
				if (open == 0) {
					open = 1;
					openDoor();
				} else {
					open = 0;
					closeDoor();
				}
			}
		}
	}

	if (cicle >= vel) {
		cicle = 1;

		++score;
		if (score >= 10000) score = 0;
		printScore(score);

		for (i = 1; i < 6; ++i)
		{
			if (GetChar(14, (i * 4) + 1) == 130)
			{
				deleteAllShield(14, i);
			}

			if (GetChar(12, (i * 4) + 1) == 130)
			{
				deleteShield(12, i);
				printShield(14, i);

				if (y == i) {
					++miss;
					if (miss > 3)
					{
						deleteCharacter(y);
						printGameOver();
						if (score > hiscore) {
							hiscore = score;
							printHiscore(hiscore);
						}
						in_Wait(250);
						goto RESET;
					}
					live(miss);
					deleteCharacter(y);
					printLiveLess();
					y = 0;
					go = 0;
					printCharacter(y);
				}
			}

			if (GetChar(10, (i * 4) + 1) == 130)
			{
				deleteShield(10, i);
				printShield(12, i);
			}
			if (GetChar(8, (i * 4) + 1) == 130)
			{
				deleteShield(8, i);
				printShield(10, i);
			}
			if (GetChar(6, (i * 4) + 1) == 130)
			{
				deleteShield(6, i);
				printShield(8, i);
			}
		}

		x = rand() % num + 1;

		if (GetChar(h, (x * 4) + 1) == 0 && GetChar(h + 2, (x * 4) + 1) == 0 && x < 6) {
			printShield(h, x);
		}
	}


	if (score >= 3000) {
		vel = 15;
	}
	else if (score >= 2500) {
		vel = 20;
	}
	else if (score >= 2000) {
		vel = 25;
	}
	else if (score >= 1500) {
		num = 5;
	}
	else if (score >= 1000) {
		vel = 50;
	}
	else if (score >= 900) {
		num = 6;
	}
	else if (score >= 750) {
		vel = 100;
	}
	else if (score >= 500) {
		num = 7;
	}
	else if (score >= 350) {
		vel = 150;
	}
	else if (score >= 150) {
		num = 7;
	}
	else if (score >= 75) {
		vel = 200;
	}
	else if (score >= 50) {
		num = 8;
	}

	goto INITGAME;

	return 0;
}