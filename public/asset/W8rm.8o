#################################################################################
#
# W8rm (salvacam 2024)
#
# Eat apples and avoid crashing into walls and your body.
# Buttons 5,8,7,9 (W,S,A,D in emulator) to move snake, any button to start game.
#
# Lots of code from:
# https://github.com/JohnEarnest/chip8Archive/blob/master/src/snake/snake.8o
#
#################################################################################

:const longBodyInit 3

:const W_KEY 5
:const A_KEY 7
:const S_KEY 8
:const D_KEY 9

:alias headx      v3
:alias heady      v4

:alias foodx      v5
:alias foody      v6

:alias dir        v7

:alias longBody   v8

:alias keyboard   v9
:alias ti         va

:alias hiScore    vb

: screen-array # array[400] -  xy pairs (200)
  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 

: gameOver
  v0 := 10
  buzzer := v0
  clear
  
  i := hi
  v0 := 0
  v1 := 6
  sprite v0 v1 5

  i := score1
  v0 += 8
  sprite v0 v1 5  
  v0 := 45
  sprite v0 v1 5  

  i := score2
  v0 := 16
  sprite v0 v1 5  
  v0 := 53
  sprite v0 v1 5  

  i := score3
  v0 := 24
  sprite v0 v1 5
  v0 := 61
  sprite v0 v1 5

  i := end11
  v0 := 14
  v1 := 18
  sprite v0 v1 5

  i := end12
  v0 += 8
  sprite v0 v1 5

  i := end21
  v0 += 12
  sprite v0 v1 5

  i := end22
  v0 += 8
  sprite v0 v1 5

  draw-score
  draw-hiscore

  #Pres Any key to reset 
  loop
    keyboard := 0
    loop
      if keyboard key then jump doneG
      keyboard += 1   
      if keyboard == 16 then keyboard := 0
    again
  again
  
  : doneG
  v0 := 30 # 60 -> 1 sec
  delay := v0

  loop
    v0 := delay
    if v0 != 0 then
  again

  # clear screen-array
  v0 := 0
  v1 := 0
  v2 := 0
  v3 := 0
  v4 := 0
  v5 := 0
  v6 := 0
  v7 := 0
  v8 := 0
  v9 := 50

  i := screen-array
  i += v8
  save v7
  v8 := 8
  loop
    i := screen-array
    i += v8
    save v7
    v9 -= 1
    #if v8 != 8 then v8 := 8
    if v9 != 0 then
  again

  v8 := 0
  init 
;

: drawimage
  v0 := 0  # x
  v1 := 0  # y
  v2 := 8  # stride
  loop
    sprite v0 v1 8
    i  += v2
    v0 += v2
    if v0 == 64 then v1 +=  8
    if v0 == 64 then v0 := 0
    if v1 != 32 then
  again
  return 
;

: initScreen
  clear
  i := inic
  drawimage
  
  #Pres Any key to reset 
  loop
    keyboard := 0
    loop
      if keyboard key then jump doneI
      keyboard += 1   
      if keyboard == 16 then keyboard := 0
    again
  again
  
  : doneI
  v0 := 30 # 60 -> 1 sec
  delay := v0

  loop
    v0 := delay
    if v0 != 0 then
  again
  clear
;

: draw-hiscore  
  v0 := longBody
  v0 -= longBodyInit
  v1 := hiScore
  vf := 1

  v1 -= v0
  if vf == 0 then hiScore := v0

  headx := 0
  heady := 0

  v0 := hiScore
  draw-points
;

: draw-points
  i := digits   # the destination for bcd
  bcd v0        # unpack digits in v0

  i := digits
  load v2        # load digits into v0-v2

  i := hex v0    # hundreds digit
  sprite headx heady 5
  
  i := hex v1    # tens digit
  headx += 6
  sprite headx heady 5

  i := hex v2    # ones digit
  headx += 6
  sprite headx heady 5
;

: draw-score
  v0 := longBody
  v0 -= longBodyInit

  headx := 48
  heady := 0

  draw-points
;

: deleteTail
  v2 := ti
  v2 -= longBody

  i := screen-array
  i += v2
  i += v2
  load v1

  i := snake
  if v1 != 0 then sprite v0 v1 3
;

: writeTail
  ti += 1 #Increase screen index
  i := screen-array
  i += ti
  i += ti
  v0 := headx
  v1 := heady
  save v1
;

: draw-apple
  vf := 3
  buzzer := vf
  i := food
  sprite foodx foody 3
  longBody += 1

  ve := random 19
  foodx := 2
  loop
    while ve != 0
    ve -= 1
    foodx += 3
  again
  
  ve := random 9
  foody := 1
  loop
    while ve != 0
    ve -= 1
    foody += 3
  again

  sprite foodx foody 3 
  jump draw-apple-return
;

: main
  hiScore := 0
  
  : init 
  initScreen

  ti := 0

  # Draw Wall
  v0 := 1
  v1 := 1
  v2 := 62
  loop
    i := wallV
    sprite v0 v1 15
    i := wallV
    sprite v2 v1 15
    v1 += 15
    if v1 != 31 then
  again
  
  v0 := 1
  v1 := 0
  v2 := 31
  loop
    i := wallH
    sprite v0 v1 1
    sprite v0 v2 1
    v0 += 8
    if v0 != 57 then
  again

  v0 := 57
  i := wallH1
  sprite v0 v1 1
  sprite v0 v2 1

  dir := 2  
  longBody := longBodyInit

  headx := 11
  heady := 7

  foodx := 44
  foody := 7

  i := food
  sprite foodx foody 3

  loop 
    vf := 0
    i := snake
    sprite headx heady 3
    writeTail
    if vf == 1 then v1 := 1
    
    #Food collision check
    v0 := 0
    if headx == foodx then v0 += 1
    
    if heady == foody then v0 += 1
  	
    if v0 == 2 then draw-apple
      if v1 == 1 then gameOver
      if headx == 255 then gameOver
    : draw-apple-return

    if v0 != 2 then deleteTail

    keyboard := 7 # left
    if dir == 2 then jump keys-else-1
    if keyboard key then dir := 4

    : keys-else-1
    keyboard := 9 # right
    if dir == 4 then jump keys-else-2
    if keyboard key then dir := 2

    : keys-else-2
    keyboard := 5 # up
    if dir == 3 then jump keys-else-3
    if keyboard key then dir := 1

    : keys-else-3
    keyboard := 8 # down
    if dir == 1 then jump keys-else-4
    if keyboard key then dir := 3

    : keys-else-4

    if dir == 1 then heady -= 3 # up
    if dir == 2 then headx += 3 # right
    if dir == 3 then heady += 3 # down
    if dir == 4 then headx -= 3 # left

  again

: food 0xE0 0xE0 0xE0
: snake 0xE0 0xA0 0xE0

: wallH 0xFF
: wallH1 0xFC
: wallV 0x80 0x80 0x80 0x80 0x80 0x80 0x80 0x80 0x80 0x80 0x80 0x80 0x80 0x80 0x80

: digits 0 0 0

: inic
  0x00 0x00 0x00 0x0C 0x0E 0x06 0x06 0x06 
  0x00 0x00 0x00 0x02 0x03 0x03 0x03 0x01 
  0x00 0x00 0x03 0xCF 0x9E 0x98 0x9C 0x9F 
  0x00 0x00 0xF0 0xF8 0x19 0x19 0x31 0xB1 
  0x00 0x00 0x3F 0xFF 0xE3 0xC1 0x81 0xC3 
  0x00 0x00 0x00 0x80 0xC0 0xC0 0xC0 0xC1 
  0x00 0x00 0x00 0x39 0x7D 0xFD 0xFF 0xCF 
  0x00 0x00 0x60 0xF8 0xF8 0x9C 0x9C 0x1C 
  0x06 0x06 0x06 0x07 0x07 0x07 0x03 0x01 
  0x39 0x7D 0xFD 0xFF 0xDF 0xCF 0x86 0x00 
  0x8F 0x87 0x8F 0x9C 0x9C 0x1C 0x0F 0x07 
  0xE1 0xF1 0x38 0x38 0x38 0x78 0xF1 0xC1 
  0xC7 0xCF 0xCE 0xEE 0xE7 0xE3 0xE1 0xC0 
  0x81 0x01 0x00 0x00 0x80 0xF9 0xF9 0x01 
  0xCF 0xC6 0xC0 0xC0 0xC0 0xC0 0xC0 0x80 
  0x18 0x18 0x38 0x38 0x38 0x38 0x18 0x18 
  0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
  0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
  0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
  0x01 0x00 0x00 0x22 0x22 0x2A 0x36 0x22 
  0x80 0x00 0x00 0x10 0x38 0x54 0x10 0x10 
  0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
  0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
  0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
  0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 
  0x00 0x00 0x1C 0x22 0x22 0x3E 0x22 0x00 
  0x00 0x00 0x10 0x20 0x7C 0x20 0x10 0x00 
  0x00 0x00 0x1E 0x20 0x1E 0x02 0x3C 0x00 
  0x00 0x00 0x10 0x10 0x54 0x38 0x10 0x00 
  0x00 0x00 0x08 0x04 0x3E 0x04 0x08 0x00 
  0x00 0x00 0x70 0x48 0x48 0x48 0x70 0x00 
  0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 

: end11  0x6E 0x8A 0xAE 0xAA 0x6A  # GA
: end12  0xAE 0xE8 0xAE 0xA8 0xAE  # ME

: end21  0x4A 0xAA 0xAA 0xAA 0x4C  # OV
: end22  0xEC 0x8A 0xEC 0x8A 0xEA  # ER

: hi     0xAE 0xA4 0xE4 0xA4 0xAE  # HI

: score1 0x66 0x88 0x68 0x28 0xC6  # SC
: score2 0x4C 0xAA 0xAC 0xAA 0x4A  # OR
: score3 0xE0 0x80 0xE0 0x80 0xE0  # E
