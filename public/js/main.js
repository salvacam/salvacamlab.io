  document.onreadystatechange = function (event) {
  if (document.readyState === 'complete') {

    var speccy = document.getElementById('speccy');
    var zx81 = document.getElementById('zx81');
    var chip8 = document.getElementById('chip8');
    var js = document.getElementById('js');
    var all = document.getElementById('all');

    speccy.addEventListener("click", function() {      
      document.querySelectorAll('.zx_spectrum').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });
      document.querySelectorAll('.zx81').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.zx80').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.chip8').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.js').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });

      speccy.classList.add("active");
      zx81.classList.remove("active");
      chip8.classList.remove("active");
      js.classList.remove("active");
      all.classList.remove("active");
    });

    zx81.addEventListener("click", function() {      
      document.querySelectorAll('.zx_spectrum').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.zx81').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });
      document.querySelectorAll('.zx80').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });
      document.querySelectorAll('.chip8').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.js').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });

      speccy.classList.remove("active");
      zx81.classList.add("active");
      chip8.classList.remove("active");
      js.classList.remove("active");
      all.classList.remove("active");
    });

    chip8.addEventListener("click", function() {      
      document.querySelectorAll('.zx_spectrum').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.zx81').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.zx80').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.chip8').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });
      document.querySelectorAll('.js').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });

      speccy.classList.remove("active");
      zx81.classList.remove("active");
      chip8.classList.add("active");
      js.classList.remove("active");
      all.classList.remove("active");
    });

    js.addEventListener("click", function() {      
      document.querySelectorAll('.zx_spectrum').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.zx81').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.zx80').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.chip8').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });
      document.querySelectorAll('.js').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });

      speccy.classList.remove("active");
      zx81.classList.remove("active");
      chip8.classList.remove("active");
      js.classList.add("active");
      all.classList.remove("active");
    });

    all.addEventListener("click", function() {      
      document.querySelectorAll('.zx_spectrum').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });
      document.querySelectorAll('.zx81').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });
      document.querySelectorAll('.zx80').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });
      document.querySelectorAll('.chip8').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });
      document.querySelectorAll('.js').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });

      speccy.classList.remove("active");
      zx81.classList.remove("active");
      chip8.classList.remove("active");
      js.classList.remove("active");
      all.classList.add("active");
    });

    var spanish = document.getElementById('spanish');
    var english = document.getElementById('english');

    spanish.addEventListener("click", function() {      
      document.querySelectorAll('.espanol').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });
      document.querySelectorAll('.ingles').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });

      spanish.classList.add("active");
      english.classList.remove("active");
    });
    
    english.addEventListener("click", function() {      
      document.querySelectorAll('.ingles').forEach(function(proyecto) {
        proyecto.classList.remove("hidden");
      });
      document.querySelectorAll('.espanol').forEach(function(proyecto) {
        addClass(proyecto, "hidden");
      });

      spanish.classList.remove("active");
      english.classList.add("active");
    });
    
    var aboutMe = document.getElementById('aboutMe');

    aboutMe.addEventListener("click", function() {
      document.getElementById('aboutMeDescription').classList.toggle("hidden");

      document.querySelectorAll('.hideAboutMe').forEach(function(proyecto) {
        proyecto.classList.toggle("hidden");
      });

      document.querySelectorAll('.showAboutMe').forEach(function(proyecto) {
        proyecto.classList.toggle("hidden");
      });
    });

    /**
    * Add class nameClass to tag element
    * @param {object} element - The tag element.
    * @param {string} nameClass - The name of class.
    */
    var addClass = function(element, nameClass) {
      if (element.classList) {
        element.classList.add(nameClass);
      } else {
        element.nameClass += ' ' + nameClass;
      }
    }

    var escapeRegExp = function(string) {
      return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
    }

    var replaceAll = function(str, match, replacement){
      return str.replace(new RegExp(escapeRegExp(match), 'g'), ()=>replacement);
    }

    /***********************************************************/
    /*******                 DATA                     **********/
    /***********************************************************/

  var proyectos = [    
    {
      'title': 'Bounce',
      'tap': 'Bounce.ch8',
      'src': 'Bounce.8o',
      'img': 'Bounce.png',
      'text': 'Rebota la pelota y elimina los bloques.',
      'textEN': 'Bounce the ball and remove the blocks.',
      'platform': 'CHIP8',
      'chip8': 'Bounce'
    },
    {
      'title': 'Wall of China',
      'tap': 'WallOfChina.ch8',
      'src': 'WallOfChina.8o',
      'img': 'WallOfChina.png',
      'text': 'Tienes que evitar ser tocado antes de llegar a la derecha de la pantalla.',
      'textEN': 'You have to avoid being touched before reaching the right of the screen.',
      'platform': 'CHIP8',
      'chip8': 'WallOfChina'
    },
    {
      'title': 'Chip Runner',
      'tap': 'ChipRunner.ch8',
      'src': 'ChipRunner.8o',
      'img': 'ChipRunner.png',
      'text': "Salta para evitar los cactus. Tecla 5 (W en emulador) para saltar y empezar.",
      'textEN': "Jump to avoid cactus. Key 5 (W in emulator) to jump and start game.",
      'platform': 'CHIP8',
      'chip8': 'ChipRunner'
    },
    {
      'title': 'W8rm',
      'tap': 'W8rm.ch8',
      'src': 'W8rm.8o',
      'img': 'W8rm.png',
      'text': "Come manzanas y evita chocar con los murosy tu cuerpo",
      'textEN': "Eat apples and avoid crashing into walls and your body",
      'platform': 'CHIP8',
      'chip8': 'W8rm'
    },
    {
      'title': 'Formula 8',
      'tap': 'Formula8.ch8',
      'src': 'Formula8.8o',
      'img': 'Formula8.png',
      'text': "Corre los maximo posible evitando chocar con los otros coches",
      'textEN': "Run as fast as possible avoiding crashing into other cars",
      'platform': 'CHIP8',
      'chip8': 'Formula8'
    },
    {
      'title': 'Sn8ke',
      'tap': 'Sn8ke.ch8',
      'src': 'Sn8ke.8o',
      'img': 'Sn8ke.png',
      'text': "Come manzanas y evita chocar con los murosy tu cuerpo",
      'textEN': "Eat apples and avoid crashing into walls and your body",
      'platform': 'CHIP8',
      'chip8': 'Sn8ke'
    },
    {
      'title': '8 Dodge',
      'tap': '8Dodge.ch8',
      'src': '8Dodge.8o',
      'img': '8Dodge.png',
      'text': "Abrerte paso a través del campo de asteroides, suma un punto por cada asteroide que esquives",
      'textEN': "Make your way through the asteroid field, scoring one point for each asteroid you dodge",
      'platform': 'CHIP8',
      'chip8': '8Dodge'
    },
    {
      'title': 'Chipway',
      'tap': 'Chipway.ch8',
      'src': 'Chipway.8o',
      'img': 'Chipway.png',
      'text': "Mi versión de Breakout para Chip-8",
      'textEN': "My version of Breakout for Chip-8",
      'platform': 'CHIP8',
      'chip8': 'chipway'
    },
    {
      'title': 'Rechip',
      'tap': 'Rechip.ch8',
      'src': 'Rechip.8o',
      'img': 'Rechip.png',
      'text': "¿Cuantas veces puedes hacer rebotar la pelota sin que salga del campo?",
      'textEN': "How many times can you bounce the ball without it going out of bounds?",
      'platform': 'CHIP8',
      'chip8': 'rechip'
    },
    {
      'title': 'Zhunder Blade',
      'tap': 'zhunder.ch8',
      'src': 'zhunder.8o',
      'img': 'zhunder.png',
      'text': "Evita los disparos enemigos y chocar con los muros. Mi primer juego para Chip-8",
      'textEN': "Avoid enemy shots and colliding with walls. My first game for Chip-8",
      'platform': 'CHIP8',
      'chip8': 'zhunder'
    },
    {
      'title': '81-tris',
      'tap': '81-tris.zip',
      'src': '81-tris.c',
      'img': '81-tris.png',
      'text': "El clásico juego Tetris con dos modos de juegos",
      'textEN': "The classic Tetris game with two game modes",
      'platform': 'ZX81',
      'zx81': '81-tris'
    },
    {
      'title': 'Ipong',
      'tap': 'Ipong.p',
      'src': 'Ipong.c',
      'img': 'Ipong.png',
      'text': "Un pong intelligente de un solo jugador. Presentado en la Crap 2023",
      'textEN': "A single player smart pong. Featured at Crap 2023",
      'platform': 'ZX81'
    },
    {
      'title': 'Snake',
      'tap': 'snake.p',
      'src': 'snake.c',
      'img': 'snake.png',
      'text': 'El clasico juego de la serpiente. Presenteado al concurso Retro Snake Game Jam',
      'textEN': 'The classic snake game. Submitted to the Retro Snake Game Jam contest',
      'platform': 'ZX81'
    },
    {
      'title': 'Pong81',
      'tap': 'pong81.p',
      'src': 'Pong81.bas',
      'img': 'pong81.png',
      'text': 'Un sencillo Pong en 10 lineas. Los controles son las teclas Q y W. Presentado al concurso 10 Liners 2023.',
      'textEN': 'A simple Pong in 10 lines. The controls are the Q and W keys. Submitted to the 10 Liners of 2023 contest.',
      'platform': 'ZX81'
    },
    {
      'title': 'Escape from Tokat dungeon',
      'tap': 'escapeFromTokatDungeon.zip',
      'src': 'escapeFromTokatDungeon.c',
      'img': 'escapeFromTokatDungeon.png',
      'text': 'Tienes que coger las llaves e ir a la salida, evitando enmigos. Presentado al concurso Retro Game Dev 2022',
      'textEN': 'You have to take the keys and go to the exit, avoiding enemies. Submitted to Retro Game Dev 2022 Contest',
      'platform': 'ZX81',
      'zx81': 'escapeFromTokatDungeon'
    },
    {
      'title': 'Palo T Game and Watch',
      'tap': 'paloT_GameAndWatch.p',
      'src': 'paloT_GameAndWatch.c',
      'img': 'paloT_GameAndWatch.png',
      'text': 'Adaptación de la Game & Watch Helmet al paloverso.',
      'textEN': 'Adaptation of the Game & Watch Helmet to the paloverso.',
      'platform': 'ZX81'
    },
    {
      'title': 'Robbers of the Lost Tomb',
      'tap': 'robbersofthelosttomb_es.p',
      'src': 'robbersofthelosttomb_es.bas',
      'img': 'robbersofthelosttomb_es.jpg',
      'text': 'Traducción al español del juego del 82 Robbers of the Lost Tomb, busca las cuatro tablillas sagradas.',
      'textEN': 'Spanish translation of the game of 82 Robbers of the Lost Tomb, look for the four sacred tablets.',
      'platform': 'ZX81'
    },
    {
      'title': 'BMX Trial',
      'tap': "bmxTrial.P",
      'src': 'bmxTrial.c',
      'img': 'bmx81.png',
      'text': "Un demake del Alex Kidd BMX Trial para ZX81",
      'textEN': "A demake of the Alex Kidd BMX Trial for ZX81",
      'platform': 'ZX81'
    },
    {
      'title': 'Umul Gonu',
      'tap': "umul_gonu.p",
      'src': 'umul_gonu.bas',
      'img': 'umul_gonu.png',
      'text': "EL famoso juego de mesa Pong Hau K'i o Umul Gonu, en coreano, para el ZX81. Presentado en la Crap 2022",
      'textEN': "The famous board game Pong Hau K'i or Umul Gonu, in Korean, for the ZX81. Featured at Crap 2022",
      'platform': 'ZX81'
    },
    {
      'title': 'Polarion Crap',
      'tap': "polarion_c.p",
      'src': 'polarion_c.bas',
      'img': 'polarion_c.png',
      'text': 'Remake sin limitaciones del juego presentado al concurso 10 Liners de 2022.',
      'textEN': 'Unrestricted remake of the game submitted to the 2022 10 Liners contest.',
      'platform': 'ZX81'
    },
    {
      'title': 'Polarion 10Liners',
      'tap': "polarion.p",
      'src': 'polarion.bas',
      'img': 'polarion.png',
      'text': 'Un juego en el que tienes que evitar caer en los huecos. Presentado al concurso 10 Liners de 2022.',
      'textEN': 'A game where you have to avoid falling into gaps. Submitted to the 10 Liners of 2022 contest.',
      'platform': 'ZX81'
    },
    {
      'title': '81sketch',
      'tap': "81sketch.p",
      'src': '81sketch.bas',
      'img': '81sketch.png',
      'text': 'Un telesketch para ZX81. Presentado al concurso 10 Liners de 2022.',
      'textEN': 'A telesketch for ZX81. Submitted to the 10 Liners of 2022 contest.',
      'platform': 'ZX81'
    },
    {
      'title': 'Amidar A',
      'tap': 'amidarA.tap',
      'src': 'amidar.bas',
      'img': 'amidar.jpg',
      'text': 'Ganador del concurso de Basic de 2022 de Bytemaniacos, categoría BASIC compilado. Modo A de juego.',
      'textEN': 'Bytemaniacos 2022 Basic Contest Winner, Compiled BASIC category. Game mode A.',
      'platform': 'ZX Spectrum'
    },    
    {
      'title': 'Amidar B',
      'tap': 'amidarB.tap',
      'src': '4th.pdf',
      'img': '4th.jpg',
      'text': 'Nominado para mejor juego en Basic del año 2022, en los premios Planeta Sinclair. Modo B de juego',
      'textEN': 'Nominated for Best Basic Game of the Year 2022, at the Planeta Sinclair Awards. Game mode B',
      'platform': 'ZX Spectrum'
    },
    {
      'title': 'Kingdom',
      'tap': 'Kingdomsp.p',
      'src': 'Kingdom_es.bas',
      'img': 'Kingdom.jpg',
      'text': 'Traducción al español del juego de 1982 Kingdom, donde tienes que gestionar tu reino medieval.',
      'textEN': 'Spanish translation of the 1982 Kingdom game, where you have to manage your medieval kingdom.',
      'platform': 'ZX81'
    },
    {
      'title': 'Trader',
      'tap': "Trader_zx81_esp.zip",
      'src': 'Trader_zx81_esp_src.zip',
      'img': 'trader.jpg',
      'text': 'Traducción al español del juego del 82 Trader, una aventura espacial, mezcla de comercio, arcade y exploración.',      
      'textEN': 'Spanish translation of the 82 Trader game, a space adventure, a mix of trading, arcade and exploration.',
      'platform': 'ZX81',
      'zx81': "Trader"
    },
    {
      'title': 'Rudolph practices',
      'tap': "rudolph.P",
      'src': 'rudolph.c',
      'img': 'rudolph.jpg',
      'text': 'Un clon del anterior juego presentado en la Crap 2021 para que acabara el concurso en un número redondo.',
      'textEN': 'A clone of the previous game presented at Crap 2021 to finish the contest in a round number.',
      'platform': 'ZX81'
    },
    {
      'title': 'Highway Robbery 2021',
      'tap': "highway_robbery.P",
      'src': 'highway_robbery.c',
      'img': 'highway_robbery.jpg',
      'text': 'Un clon de un juego argentino, al que le añado Hi-score. Presentado en la Crap 2021',
      'textEN': 'A clone of an Argentinian game, to which I add Hi-score. Presented at Crap 2021',
      'platform': 'ZX81'
    },
    {
      'title': 'Escarabajo',
      'tap': "escarabajo.p",
      'src': 'escarabajo.bas',
      'img': 'escarabajo.jpg',
      'text': 'Un demake para ZX81 de un juego de la revista ZX. Presentado en la Crap 2021',
      'textEN': 'A ZX81 demake of a ZX magazine game. Presented at Crap 2021',
      'platform': 'ZX81'
    },
    {
      'title': 'Fall Pallo T',
      'tap': "fallPalot.P",
      'src': 'fallPalot.c',
      'img': 'fallPalot.jpg',
      'text': 'Un, intento de, clon del Nohzdyve. Presentado en la Crap 2021',
      'textEN': 'An, attempted, clone of the Nohzdyve. Presented at Crap 2021',
      'platform': 'ZX81'
    },
    {
      'title': 'Wall of China',
      'tap': "wall.P",
      'src': 'wall.c',
      'img': 'wall.jpg',
      'text': 'Tienes que evitar ser tocado antes de llegar al final de la pantalla. Presentado en la Crap 2021',
      'textEN': 'You have to avoid being touched before reaching the bottom of the screen. Presented at Crap 2021',
      'platform': 'ZX81'
    },
    {
      'title': 'Snail Maze',
      'tap': "snailMaze.P",
      'src': 'snailMaze.zip',
      'img': 'snailMaze.jpg',
      'text': 'Un demake del Snail Maze de Master System, con sorpresa final. Presentado en la Crap 2021',
      'textEN': "A demake of the Master System's Snail Maze, with a surprise ending. Presented at Crap 2021",
      'platform': 'ZX81'
    },
    {
      'title': 'Scroll',
      'tap': "scroll.p",
      'src': 'scroll.bas',
      'img': 'scroll.jpg',
      'text': 'Juego presenta al concurso ASCII BASIC GAME 10Liner',
      'textEN': 'Game Submits ASCII BASIC GAME 10Liner Contest',
      'platform': 'ZX81'
    },
    {
      'title': 'Tic Tac Toe Hell',
      'tap': "ticTacToeHell.O",
      'src': 'ticTacToeHell.c',
      'img': 'ticTacToeHell.jpg',
      'text': 'Primer juego para ZX80, usando z88dk. Presentadoo en la Crap 2021',
      'textEN': 'First ZX80 game, using z88dk. Presented at Crap 2021',
      'platform': 'ZX80'
    },
    {
      'title': 'Zhunder Vlade Z81',
      'tap': "zhunderVlade.p",
      'src': 'zhunderVladeZ81.zip',
      'img': 'zhunderVladeZ81.jpg',
      'text': 'Primer juego realizado con z88dk, reutilizo un juego de la Crap 2011 para la Crap 2021',
      'textEN': 'First game made with z88dk, I reuse a game from Crap 2011 for Crap 2021',
      'platform': 'ZX81'
    },
    {
      'title': 'Camel Racing',
      'tap': "camelracing.p",
      'src': '018.zip',
      'img': 'camelracing.png',
      'text': 'Un simulador de la atracción de feria "Carreras de Camellos". Presentado en la Crap 2021',
      'textEN': 'A simulator of the "Camel Racing" amusement ride. Presented at Crap 2021',
      'platform': 'ZX81'
    },
    {
      'title': 'You are Offline',
      'tap': "You_are_offline.p",
      'src': 'you_are_offline.zip',
      'img': 'You_are_offline.png',
      'text': 'Mi primer juego para ZX81. Presentado al concurso 10 Liners de 2021.',
      'textEN': 'My first game for ZX81. Submitted to the 10 Liners of 2021 contest.',
      'platform': 'ZX81'
    },
    {
      'title': 'Mazmorras de Tokat',
      'tap': "tokat_dx.tap",
      'src': 'tokat_dx.bas',
      'img': 'tokat.jpg',
      'text': 'Juego presentado al concurso de Mazmorras de 2020 de Bytemaniacos. Versión deluxe, incluye UDG.',
      'textEN': 'Game submitted to the 2020 Bytemaniacos Dungeon Contest. Deluxe version, includes UDG.',
      'platform': 'ZX Spectrum'
    },
    {
      'title': 'Saltarin',
      'tap': "saltarin.tzx",
      'src': 'saltarin.zip',
      'img': 'saltarin.jpg',
      'text': 'Juego de plataformas, muy frenetico, realizado con ZX Basic de Boriel, en el 2011.',
      'textEN': "Very frenetic platform game made with Boriel's ZX Basic in 2011.",
      'platform': 'ZX Spectrum'
    },
    {
      'title': 'Ratul and Zeki',
      'tap': "ratul_and_zeki.tzx",
      'src': 'ratul_and_zeki.bas',
      'img': 'ratulZeki.jpg',
      'text': 'Primer juego que realice con ZX Basic de Boriel, en el 2011, esta basado en el Binary Land de NES.',
      'textEN': "First game I made with Boriel's ZX Basic, in 2011, it is based on the NES Binary Land.",
      'platform': 'ZX Spectrum'
    },
    {
      'title': 'Roque',
      'tap': "roque.tzx",
      'src': 'roque.bas',
      'img': 'roque.png',
      'text': 'Un simulador de rueda de hámster, presentado en la Crap Games del 2011.',
      'textEN': 'A hamster wheel simulator, submitted to Crap Games 2011.',
      'platform': 'ZX Spectrum'
    },
    {
      'title': 'Mini Cirsa',
      'tap': "mini cirsa.tap",
      'src': 'mini cirsa.bor',
      'img': 'miniCirsa.png',
      'text': 'Un juego de tragaperras, presentado en la Crap Games del 2011.',
      'textEN': 'A slot game, submitted to Crap Games 2011.',
      'platform': 'ZX Spectrum'
    },
    {
      'title': 'Zhunder Vlade',
      'tap': "zhunder vlade.tap",
      'src': 'zhunder vlade.bor',
      'img': 'zhunderVlade.jpg',
      'text': 'Un juego de helicóptero, presentado en la Crap Games del 2011.',
      'textEN': 'A helicopter game, submitted to Crap Games 2011.',
      'platform': 'ZX Spectrum'
    },
    {
      'title': 'Invad3s',
      'tap': "invad3s2020.tap",
      'src': 'Invad3s2020.zip',
      'img': 'invad3s2020.jpg',
      'text': 'Juego presentado para el concurso de Baisc de 2010 de Bytemaniacos. Versión del 2020 que arregla un glitch.',
      'textEN': "Game submitted for Bytemaniacos Baisc 2010 contest. This is a version of 2020 fixing a glitch.",
      'platform': 'ZX Spectrum'
    },
    {
      'title': 'Jerusalem Lot',
      'tap': "Jerusalem_Lot.z80",
      'src': 'Jerusalem_Lot.zip',
      'img': 'Jerusalem_Lot.jpg',
      'text': 'Juego presentado para el concurso de Aventura de 2009 de Bytemaniacos. Un juego tipo Elige tu propio aventura.',
      'textEN': "Game submitted for the 2009 Bytemaniacos Adventure Contest. It's a Choose Your Own Adventure type game.",
      'platform': 'ZX Spectrum'
    },
    {
      'title': 'Mooc',
      'src': 'mooc.zip',
      'tap': 'mooc.zip',
      'img': 'mooc.png',
      'text': "Juego desarrollado para el MOOC Creando Apps. Aprende a programar aplicaciones móviles.",
      'textEN': "Game developed for the MOOC Creating Apps. Learn to program mobile applications.",
      'platform': 'JS',
      'js': 'mooc'
    },
    {
      'title': 'Barquitos',
      'src': 'barquitos.zip',
      'tap': 'barquitos.zip',
      'img': 'barquitos.png',
      'text': "Hundir la flota en JavaScript",
      'textEN': "Battleship in JavaScript",
      'platform': 'JS',
      'js': 'barquitos'
    },
    {
      'title': 'Plankton Invaders',
      'src': 'planktonInvaders.zip',
      'tap': 'planktonInvaders.zip',
      'img': 'planktonInvaders.png',
      'text': "Clon del Space Invader, prueba del framework Phaser",
      'textEN': "Space Invader clone, Phaser framework test",
      'platform': 'JS',
      'js': 'planktonInvaders'
    }
  ];

    /***********************************************************/
    /*******                 TEMPLATE                 **********/
    /***********************************************************/


    var ifSimple = function(template, object, key) {
      if(object[key]) {
        var startPos = template.indexOf("{%if "+key);
        var endPos = template.indexOf("%}", startPos);
        var borrar = template.substring(startPos, endPos + 2);
        template = template.replace(borrar, "");

        template = replaceText(template, key, object[key]);
        template = template.replace("{%endif%}", "");
      } else {

        var startPos = template.indexOf("{%if");
        var endPos = template.indexOf("{%endif%}");
        var borrar = template.substring(startPos, endPos + 9);
        template = template.replace(borrar, "");
      }
      return template;
    }

    var ifSimpleTemplate = function(template, object) {
      for(var key in object) {
        template = ifSimple(template, object, key);
      }
      return template;
    }

    var replaceText = function(content, key, value) {
      //return content.replaceAll("{{"+ key +"}}", value ? value : "");
      return replaceAll(content, "{{"+ key +"}}", value ? value : "");
    }

    var replaceTemplate = function(template, object) {
      for(var key in object) {
        template = replaceText(template, key, object[key]);
      }
      return template;
    }

    /*    Render template     */

    proyectos.forEach(function(project) {
      var template = document.getElementById("gameTemplate").innerHTML;

      template = replaceTemplate(template, {
        "img": project.img,
        "title": project.title,
        "src": project.src,
        "tap": project.tap,
        "text": project.text,
        "textEN": project.textEN,        
        "platform": project.platform
      });

      let speccy = undefined;
      let zx81 = undefined;
      let chip8 = undefined;
      let js = undefined;

      if (project.js)
      {
        js = project.js;
      } 
      else if (project.chip8)
      {
        chip8 = project.chip8;
      } 
      else if (project.zx81)
      {
        zx81 = project.zx81;
      } 
      else if (project.tap.indexOf(".zip") === -1 && project.tap.indexOf(".o") === -1 && project.tap.indexOf(".O") === -1 
        && project.tap.indexOf(".P") === -1 && project.tap.indexOf(".p") === -1) 
      {
        speccy = project.tap;
      } 
      else if (project.tap.indexOf(".zip") === -1 && project.tap.indexOf(".o") === -1 && project.tap.indexOf(".O") === -1 
        && (project.tap.indexOf(".P") !== -1 || project.tap.indexOf(".p") !== -1)) {
          zx81 = project.tap.split('.').slice(0, -1).join('.');
      }

      if (project.src.indexOf(".pdf") === -1) 
      {
        template = replaceTemplate(template, {
          "Code": "Code",
          "Codigo": "Código"
        });
      } 
      else {
        template = replaceTemplate(template, {
          "Code": "Certificate",
          "Codigo": "Certificado"
        });
      }

      template = ifSimpleTemplate(template, {
        "speccy": speccy,
        "zx81": zx81,
        "chip8": chip8,
        "js": js
      });

      template = replaceTemplate(template, {
        "title": project.title
      });

      var el = document.createElement('div');
      addClass(el, "proyecto");
      var str = project.platform;
      str = str.replace(/\s+/g, '_').toLowerCase();
      addClass(el, str);
      
      el.innerHTML = template;

      document.querySelector('#porfolio-content').appendChild(el);
    });

  }

}
  
